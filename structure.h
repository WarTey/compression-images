#define NB_CHAR 100         // Nombres de caractères dans une chaîne de caractères
#define NB_IMG 12           // Nombres d'images sauvegardées
#define NB_BOUTONS 12       // Nombres de boutons sur l'interface
#define DIM_IMG_MAX 512     // Dimensions maximales de l'image
#define AFF_ARBRE_BIN 0     // 1 - Affiche l'arbre en console
                            // 0 - N'affiche pas l'arbre en console
#define SAVE_ETIQ 1         // 1 - Sauvegarde l'image binaire avec la couleur des étiquettes
                            // 0 - Sauvegarde l'image binaire en noir et blanc

typedef char chaine[NB_CHAR];

// Contient les différentes transformation d'une image faites au cours du programme
typedef struct matImage {
    int largeurImage, hauteurImage;
    int seuil;
    short int **rouge, **vert, **bleu;
	short int **rougeInverse, **vertInverse, **bleuInverse;
	short int **niveauGris;
	short int **seuillage;
	short int **filtreMedian;
    short int **resultatBinaire;
} matImage;

// Contient les données des chaînes de caractères
// (texte d'erreur lecture, images à traiter/sauvegarder et taille)
typedef struct nomImage {
    chaine erreurDimension, erreurLecture;
	int tailleErreur;
	chaine nomImage[NB_IMG];
} nomImage;

// Contient les coordonnées xmin, ymin, xmax, ymax
typedef struct coordonnees {
    float xmin, ymin, xmax, ymax;
} coordonnees;

// Contient les données d'un bouton (position, texte et taille)
typedef struct bouton {
    int index, tailleTexte;
    chaine couleur[NB_BOUTONS];
} bouton;

// Représente un noeud de l'arbre
typedef struct noeudBinaire {
    int valeur, etiq;
    struct noeudBinaire *filsNO, *filsNE, *filsSE, *filsSO;
    int xmin, ymin, xmax, ymax;
} noeudBinaire;

// Repésente un noeud de l'arbre compressé
typedef struct arbreImage {
    int xmin, ymin, xmax, ymax;
    short int **imageDivisee;
    struct arbreImage *filsNO, *filsNE, *filsSO, *filsSE;
} arbreImage;