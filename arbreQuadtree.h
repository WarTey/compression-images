void calculCarreNB(short int **image2Arbre, noeudBinaire **arbre);

void creeArbreNB(short int **imageModif, noeudBinaire **arbre, int xmin, int ymin, int xmax, int ymax);

void etiquetteFeuilleNB(noeudBinaire **arbre, int *etiq);

void afficheArbre(noeudBinaire *arbre);

void creeMatriceArbreNB(noeudBinaire *arbre, short int ***resultat, int etiq);

void libereArbreNB(noeudBinaire **arbre);
