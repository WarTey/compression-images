main: main.o gereMatrice.o traitement.o gereMenu.o arbreQuadtree.o arbreHaar.o compressionBinaire.o fusionQuantification.o libisentlib.a
# Linux
	gcc -Wall main.o gereMatrice.o traitement.o gereMenu.o arbreQuadtree.o arbreHaar.o compressionBinaire.o fusionQuantification.o -o main libisentlib.a -lm -lglut -lGL -lX11
# MacOSX
#	gcc -Wall main.o gereMatrice.o traitement.o gereMenu.o arbreQuadtree.o arbreHaar.o compressionBinaire.o fusionQuantification.o -o main libisentlib.a -lm -framework OpenGL -framework GLUT

main.o: main.c GfxLib.h BmpLib.h ESLib.h structure.h gereMenu.h
	gcc -Wall -c main.c

gereMatrice.o: gereMatrice.c gereMatrice.h BmpLib.h structure.h
	gcc -Wall -c gereMatrice.c

traitement.o: traitement.c traitement.h BmpLib.h structure.h
	gcc -Wall -c traitement.c

gereMenu.o: gereMenu.c gereMenu.h GfxLib.h BmpLib.h structure.h gereMatrice.h traitement.h arbreQuadtree.h arbreHaar.h compressionBinaire.h fusionQuantification.h
	gcc -Wall -c gereMenu.c

arbreQuadtree.o: arbreQuadtree.c arbreQuadtree.h structure.h BmpLib.h
	gcc -Wall -c arbreQuadtree.c

arbreHaar.o: arbreHaar.c arbreHaar.h BmpLib.h structure.h gereMatrice.h
	gcc -Wall -c arbreHaar.c

compressionBinaire.o: compressionBinaire.c compressionBinaire.h BmpLib.h structure.h gereMatrice.h
	gcc -Wall -c compressionBinaire.c

fusionQuantification.o: fusionQuantification.c fusionQuantification.h structure.h BmpLib.h gereMatrice.h compressionBinaire.h
	gcc -Wall -c fusionQuantification.c

libisentlib.a: BmpLib.o ErreurLib.o ESLib.o GfxLib.o OutilsLib.o
	ar r libisentlib.a BmpLib.o ErreurLib.o ESLib.o GfxLib.o OutilsLib.o
	ranlib libisentlib.a

BmpLib.o: BmpLib.c BmpLib.h OutilsLib.h
	gcc -Wall -O2 -c BmpLib.c

ESLib.o: ESLib.c ESLib.h ErreurLib.h
	gcc -Wall -O2 -c ESLib.c

ErreurLib.o: ErreurLib.c ErreurLib.h
	gcc -Wall -O2 -c ErreurLib.c

GfxLib.o: GfxLib.c GfxLib.h ESLib.h
# Linux
	gcc -Wall -O2 -c GfxLib.c -I/usr/include/GL
# MacOSX
#	gcc -Wall -O2 -c GfxLib.c -Wno-deprecated-declarations

OutilsLib.o: OutilsLib.c OutilsLib.h
	gcc -Wall -O2 -c OutilsLib.c

clean:
	rm -f *~ *.o images/Copie.bmp images/Negatif.bmp images/Gris.bmp images/Seuil.bmp images/Median.bmp images/ArbreBinaire.bmp images/CompressionHaar.bmp images/BinaireSansPerte.bmp images/NGSansPerte.bmp images/BinaireAvecPerte.bmp images/NGAvecPerte.bmp imageBinaire.bin imageNG.bin imageBinaireOpt.bin imageNG_AP_K.bin

deepclean: clean
	rm -f main libisentlib.a
