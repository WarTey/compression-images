void cree3Matrices(DonneesImageRGB *image, short int ***rouge, short int ***vert, short int ***bleu);

void creeMatrice(DonneesImageRGB *image, short int ***image2Arbre);

void libere3Matrices(int hauteurImage, short int ***rouge, short int ***vert, short int ***bleu);

void libereMatrice(int hauteurImage, short int ***image2Arbre);

void creeImage(DonneesImageRGB **image, short int **rouge, short int **vert, short int **bleu, int hauteurImage, int largeurImage);
