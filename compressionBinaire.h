void sauveFeuilleBinaire(noeudBinaire *noeud, FILE *f);

void compressionBinaireSP(noeudBinaire *noeud);

void sauveMatriceBinaire(short int ***recupImage, FILE *f);

void decompressionBinaireSP(short int ***recupImage);

void placeOptimise(short int *tabZigzag, int taille, int *nbOptimisation);

void optimiseTableau(short int **tabZigzagOpti, unsigned short int **tabZigzagIte, short int *tabZigzag, int taille);

void sauveFeuilleNG(arbreImage *noeud, FILE *f);

void compressionNG_SP(arbreImage *noeud);

void parcourtZigzag(short int **imageDivisee, short int **tabZigzag, int i, int j, int indice, int taille);

void recompositionZigzag(short int ***imageDivisee, short int *tabZigzag, int i, int j, int indice, int taille);

void deOptimiseTableau(short int **tabZigzag, short int *tabZigzagOpti, unsigned short int *tabZigzagIte, int taille);

void sauveMatriceNG(short int ***recupImage, FILE *f);

void decompressionNG_SP(short int ***recupImage);

void libereTableauZigzag(short int **tabZigzag, short int **tabZigzagOpti, unsigned short int **tabZigzagIte);
