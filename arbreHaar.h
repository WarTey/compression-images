void alloueFils(arbreImage **noeud, int xmin, int ymin, int xmax, int ymax);

void decomposeImage(arbreImage **noeud, int largeurImage, int hauteurImage);

void decomposeNniveaux(matImage *imageModif, arbreImage **noeud, int niveaux);

void sauveArbreImage(DonneesImageRGB **image, arbreImage **noeud, int niveaux, int compteur);

void recomposeImage(arbreImage **noeud);

void libereArbre(arbreImage **noeud);

void libereFilsNO(arbreImage **noeud);
