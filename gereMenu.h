void initImage(nomImage *imageInfo);

void initImageTemoin(DonneesImageRGB **imageTemoin, DonneesImageRGB *image);

void initBouton(bouton *boutonInfos);

void initMatrices(matImage **imageModif, DonneesImageRGB **image, chaine nomImage);

void initArbre(noeudBinaire **arbre, matImage *imageModif);

void creeArbreBinaire(matImage *imageModif, noeudBinaire **arbre);

void creeArbreHaar(DonneesImageRGB **image, matImage *imageModif, arbreImage **arbreHaar);

void libereMatrices(matImage **imageModif, DonneesImageRGB **image, DonneesImageRGB **imageTemoin, noeudBinaire **arbre);

void traitementBinaire(matImage **imageModif, noeudBinaire **arbre);

void affichage(DonneesImageRGB *image, DonneesImageRGB *imageTemoin, nomImage imageInfo, bouton boutonInfos, int erreurDimension);

int niveauDecompositionHaar();

int facteurQualite();

int niveauReconstitutionHaar(int niveauDecomposition);

void compressionDecompressionBinaireSP(DonneesImageRGB **image, matImage *imageModif, int *index, noeudBinaire **arbre, arbreImage **arbreHaar);

void optimisationCompressionDecompression(DonneesImageRGB **image, matImage *imageModif, int *index, noeudBinaire **arbre, arbreImage **arbreHaar);

void clicMenu(DonneesImageRGB **image, matImage *imageModif, int *index, noeudBinaire **arbre, arbreImage **arbreHaar);

void changementSeuil(DonneesImageRGB **image, matImage *imageModif, int *seuil, int index);

void sauvegarde(DonneesImageRGB *image, nomImage imageInfo, int index);
