#include <stdlib.h>     // Pour pouvoir utiliser exit()
#include "BmpLib.h"     // Cet include permet de manipuler des fichiers BMP
#include "structure.h"
#include "gereMatrice.h"

// Transforme les données du champ donneesRGB d'une structure DonneesImageRGB *image
// en trois matrices de short int rouge, vert et bleu
void cree3Matrices(DonneesImageRGB *image, short int ***rouge, short int ***vert, short int ***bleu) {
    // Copie la hauteur et largeur de l'image dans deux variables locales
    int hauteurImage = image->hauteurImage, largeurImage = image->largeurImage;

    // Initialise dynamiquement les trois matrices rouge, vert et bleu
    (*rouge) = (short int **) malloc(sizeof(short int *) * hauteurImage);
    (*vert) = (short int **) malloc(sizeof(short int *) * hauteurImage);
    (*bleu) = (short int **) malloc(sizeof(short int *) * hauteurImage);
    for(int i = 0; i < hauteurImage; i++) {
        (*rouge)[i] = (short int *) malloc(sizeof(short int) * largeurImage);
        (*vert)[i] = (short int *) malloc(sizeof(short int) * largeurImage);
        (*bleu)[i] = (short int *) malloc(sizeof(short int) * largeurImage);
    }

    // Copie les valeurs de donneesRGB vers les trois matrices rouge, vert et bleu
    for(int i = 0; i < hauteurImage; i++) {
        for(int j = 0; j < largeurImage; j++) {
            (*bleu)[i][j] = image->donneesRGB[(i * largeurImage + j) * 3];
            (*vert)[i][j] = image->donneesRGB[(i * largeurImage + j) * 3 + 1];
            (*rouge)[i][j] = image->donneesRGB[(i * largeurImage + j) * 3 + 2];
        }
    }
}

// Transforme les données du champ donneesRGB d'une structure DonneesImageRGB *image
// en une matrice de short int image2Arbre
void creeMatrice(DonneesImageRGB *image, short int ***image2Arbre) {
    // Copie la hauteur et largeur de l'image dans deux variables locales
    int hauteurImage = image->hauteurImage, largeurImage = image->largeurImage;

    // Initialise dynamiquement la matrice image2Arbre
    (*image2Arbre) = (short int **)malloc(sizeof(short int *) * hauteurImage);
    for (int i = 0; i < hauteurImage; i++)
        (*image2Arbre)[i] = (short int *)malloc(sizeof(short int) * largeurImage);

    // Copie les valeurs de donneesRGB vers la matrice image2Arbre
    for (int i = 0; i < hauteurImage; i++)
        for (int j = 0; j < largeurImage; j++)
            (*image2Arbre)[i][j] = image->donneesRGB[(i * largeurImage + j) * 3];
}

// Libère proprement les matrices créées par cree3Matrices
void libere3Matrices(int hauteurImage, short int ***rouge, short int ***vert, short int ***bleu) {
    // Désallocation des trois matrices rouge, vert et bleu avec affectation de la valeur NULL
    for(int i = 0; i < hauteurImage; i++) {
        free((*rouge)[i]);
        (*rouge)[i] = NULL;
        free((*vert)[i]);
        (*vert)[i] = NULL;
        free((*bleu)[i]);
        (*bleu)[i] = NULL;
    }

    free((*rouge));
    (*rouge) = NULL;
    free((*vert));
    (*vert) = NULL;
    free((*bleu));
    (*bleu) = NULL;
}

// Libère proprement la matrice créée par creeMatrice
void libereMatrice(int hauteurImage, short int ***image2Arbre) {
    // Désallocation de la matrice image2Arbre avec affectation de la valeur NULL
    for(int i = 0; i < hauteurImage; i++) {
        free((*image2Arbre)[i]);
        (*image2Arbre)[i] = NULL;
    }

    free((*image2Arbre));
    (*image2Arbre) = NULL;
}

// Transforme trois matrices de (2^N x 2^N) valeurs entières en DonneesImageRGB *image
void creeImage(DonneesImageRGB **image, short int **rouge, short int **vert, short int **bleu, int hauteurImage, int largeurImage) {
    // On libère la variable image si celle-ci contient des données
    // pour éviter de faire de allocation excessive
    if((*image) != NULL)
        libereDonneesImageRGB(image);

    // Initialise dynamiquement la variable image ainsi que le tableau contenant les données de l'image
    (*image) = (DonneesImageRGB *) malloc(sizeof(DonneesImageRGB));
    (*image)->donneesRGB = (unsigned char *) malloc(sizeof(unsigned char) * hauteurImage * largeurImage * 3);

    // Copie la largeur et hauteur en paramètres dans la variable image
    (*image)->largeurImage = largeurImage;
    (*image)->hauteurImage = hauteurImage;

    // Copie les valeurs des trois matrices rouge, vert et bleu vers donneesRGB
    for(int i = 0; i < hauteurImage; i++) {
        for(int j = 0; j < largeurImage; j++) {
            (*image)->donneesRGB[(i * largeurImage + j) * 3] = bleu[i][j];
            (*image)->donneesRGB[(i * largeurImage + j) * 3 + 1] = vert[i][j];
            (*image)->donneesRGB[(i * largeurImage + j) * 3 + 2] = rouge[i][j];
        }
    }
}
