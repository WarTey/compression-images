#include <stdlib.h>     // Pour pouvoir utiliser exit()
#include "BmpLib.h"     // Cet include permet de manipuler des fichiers BMP
#include "structure.h"
#include "traitement.h"

// Crée les trois matrices correspondant à l'image en négatif, à partir des matrices d'une image donnée
void negatifImage(short int **rouge, short int **vert, short int **bleu, short int ***rougeInverse, short int ***vertInverse, short int ***bleuInverse, int hauteurImage, int largeurImage) {
    // Initialise dynamiquement les trois matrices rougeInverse, vertInverse et bleuInverse
    (*rougeInverse) = (short int **) malloc(sizeof(short int *) * hauteurImage);
    (*vertInverse) = (short int **) malloc(sizeof(short int *) * hauteurImage);
    (*bleuInverse) = (short int **) malloc(sizeof(short int *) * hauteurImage);
    for(int i = 0; i < hauteurImage; i++) {
        (*rougeInverse)[i] = (short int *) malloc(sizeof(short int) * largeurImage);
        (*vertInverse)[i] = (short int *) malloc(sizeof(short int) * largeurImage);
        (*bleuInverse)[i] = (short int *) malloc(sizeof(short int) * largeurImage);
    }

    // Affecte les valeurs des trois matrices (255 - rouge), (255 - vert) et (255 - bleu)
    // aux trois matrices rougeInverse, vertInverse et bleuInverse
    for(int i = 0; i < hauteurImage; i++) {
        for(int j = 0; j < largeurImage; j++) {
            (*bleuInverse)[i][j] = 255 - bleu[i][j];
            (*vertInverse)[i][j] = 255 - vert[i][j];
            (*rougeInverse)[i][j] = 255 - rouge[i][j];
        }
    }
}

// Crée la matrice en niveaux de gris correspondant aux trois matrices RVB d'une image couleur
void couleur2NG(short int **rouge, short int **vert, short int **bleu, short int ***niveauGris, int hauteurImage, int largeurImage) {
    // Initialise dynamiquement la matrice niveauGris
    (*niveauGris) = (short int **) malloc(sizeof(short int *) * hauteurImage);
    for(int i = 0; i < hauteurImage; i++)
        (*niveauGris)[i] = (short int *) malloc(sizeof(short int) * largeurImage);

    // Affecte la valeur des trois matrices (0.2125 x rouge + 0.7154 x vert + 0.0721 x bleu)
    // à la matrice niveauGris
    for(int i = 0; i < hauteurImage; i++)
        for(int j = 0; j < largeurImage; j++)
            (*niveauGris)[i][j] = 0.2125 * rouge[i][j] + 0.7154 * vert[i][j] + 0.0721 * bleu[i][j];
}

// Crée la matrice correspondant à l'image seuillée
void seuillageNG(short int **niveauGris, short int ***seuillage, int seuil, int hauteurImage, int largeurImage) {
    // Initialise dynamiquement la matrice seuillage dans le cas où cette dernière est NULL
    if((*seuillage) == NULL) {
        (*seuillage) = (short int **) malloc(sizeof(short int *) * hauteurImage);
        for(int i = 0; i < hauteurImage; i++)
            (*seuillage)[i] = (short int *) malloc(sizeof(short int) * largeurImage);
    }

    // Les valeurs de la matrice niveauGris supérieures au seuil deviennent égales à 255,
    // tandis que les valeurs inférieurs prennent la valeur 0
    for(int i = 0; i < hauteurImage; i++) {
        for(int j = 0; j < largeurImage; j++) {
            if(niveauGris[i][j] > seuil)
                (*seuillage)[i][j] = 255;
            else
                (*seuillage)[i][j] = 0;
        }
    }
}

// Tri un tableau passé en paramètre pour déterminer la valeur médiane
int triTab(int *tab, int taille) {
    // Déclaration de variables locales
    int temp = 0, indice = 0;

    // Echange deux valeurs voisines de place si celle de gauche est supérieure à celle de droite
    while(indice < taille - 1) {
        if(tab[indice] > tab[indice + 1]) {
            temp = tab[indice];
            tab[indice] = tab[indice + 1];
            tab[indice + 1] = temp;
            indice = 0;
        } else
            indice++;
    }

    // Retourne la valeur médiane
    return tab[4];
}

// Permet d'obtenir l'image filtrée d'une image en niveaux de gris,
// en utilisant une fenêtre de dimension 3 (prise en compte des 8 voisins d'un point donné)
void filtreMedian3x3(short int **niveauGris, short int ***filtreMedian, int hauteurImage, int largeurImage) {
    // Déclaration d'un tableau de dimension 9 local
    int diamensionTab = 9, *tab = (int *)malloc(sizeof(int) * diamensionTab);
    for (int i = 0; i < diamensionTab; i++)
        tab[i] = 0;

    // Initialise dynamiquement la matrice filtreMedian
    (*filtreMedian) = (short int **) malloc(sizeof(short int *) * hauteurImage);
    for(int i = 0; i < hauteurImage; i++)
        (*filtreMedian)[i] = (short int *) malloc(sizeof(short int) * largeurImage);

    // Affecte la valeur médiane à la matrice filtreMedian à l'aide de la fonction triTab
    for(int i = 1; i < hauteurImage - 1; i++) {
        for(int j = 1; j < largeurImage - 1; j++) {
            tab[0] = niveauGris[i][j];
            tab[1] = niveauGris[i-1][j-1];
            tab[2] = niveauGris[i-1][j];
            tab[3] = niveauGris[i-1][j+1];
            tab[4] = niveauGris[i][j+1];
            tab[5] = niveauGris[i][j-1];
            tab[6] = niveauGris[i+1][j-1];
            tab[7] = niveauGris[i+1][j];
            tab[8] = niveauGris[i+1][j+1];
            (*filtreMedian)[i][j] = triTab(tab, 9);
        }
    }
}
