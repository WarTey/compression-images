#include <stdio.h>		// Pour pouvoir utiliser printf()
#include <stdlib.h> 	// Pour pouvoir utiliser exit()
#include <string.h> 	// Pour pouvoir utiliser malloc()
#include "BmpLib.h" 	// Cet include permet de manipuler des fichiers BMP
#include "structure.h"
#include "arbreQuadtree.h"

// Retourne la valeur associée à un carré (1 si blanc, 0 si noir et 2 si not a color)
void calculCarreNB(short int **seuillage, noeudBinaire **arbre) {
	// Déclaration des variables locales i et j
	int i = (*arbre)->ymin, j;

	// On parcourt le morceau d'image carré (en hauteur) contenu dans noeudBinaire
	while(i < (*arbre)->ymax && (*arbre)->valeur != 2) {
		j = (*arbre)->xmin;
		// On parcourt le morceau d'image carré (en largeur) contenu dans noeudBinaire
		while(j < (*arbre)->xmax && (*arbre)->valeur != 2) {
			// On compare la couleur du pixel avec le premier pixel du carré
			// La valeur 2 est affectée si les deux pixels ne sont pas de même couleurs
			if (seuillage[(*arbre)->ymin][(*arbre)->xmin] != seuillage[i][j])
				(*arbre)->valeur = 2;
			j++;
		}
		i++;
	}

	// Dans le cas où le carré contient uniquement des pixels de même couleurs
	if((*arbre)->valeur != 2) {
		// La valeur 1 est affecté si les deux pixels sont blancs
		if (seuillage[(*arbre)->ymin][(*arbre)->xmin] == 255)
			(*arbre)->valeur = 1;
		// La valeur 0 est affecté si les deux pixels sont noirs
		else
			(*arbre)->valeur = 0;
	}
}

// Permet de créer l'arbre quaternaire à partir d'une matrice donnée représentant une image binaire
void creeArbreNB(short int **imageModif, noeudBinaire **arbre, int xmin, int ymin, int xmax, int ymax) {
	// Initialise dynamiquement le noeud arbre
	(*arbre) = (noeudBinaire *)malloc(sizeof(noeudBinaire));

	// Stock les coordonnées de l'image (2^N x 2^N) dans le noeud arbre
	(*arbre)->xmin = xmin;
	(*arbre)->xmax = xmax;
	(*arbre)->ymin = ymin;
	(*arbre)->ymax = ymax;

	// Complète le noeud de l'arbre avec la valeur associée à un carré
	calculCarreNB(imageModif, arbre);
	// Initialise l'étiquette du noeud
	(*arbre)->etiq = 0;

	if((*arbre)->valeur == 2) {
		// On réitère le processus de creeAbreNB pour chaque fils (NO, NE, SO et SE)
		// si la matrice imageModif n'est pas homogène dans ses couleurs
		creeArbreNB(imageModif, &((*arbre)->filsNO), xmin, (ymin + ymax) / 2, (xmin + xmax) / 2, ymax);
		creeArbreNB(imageModif, &((*arbre)->filsNE), (xmin + xmax) / 2, (ymin + ymax) / 2, xmax, ymax);
		creeArbreNB(imageModif, &((*arbre)->filsSO), xmin, ymin, (xmin + xmax) / 2, (ymin + ymax) / 2);
		creeArbreNB(imageModif, &((*arbre)->filsSE), (xmin + xmax) / 2, ymin, xmax, (ymin + ymax) / 2);
	} else {
		// Dans le cas inverse, lorsque que les couleurs de imageModif sont homogènes,
		// on met les fils à NULL puisqu'il s'agit d'une feuille
		(*arbre)->filsNO = NULL;
		(*arbre)->filsNE = NULL;
		(*arbre)->filsSO = NULL;
		(*arbre)->filsSE = NULL;
	}
}

// Permet d'atttribuer à chacune des feuille un numéro (étiquette) différent
// Les numéros s'incrémentent de 20 en 20 à partir de 0, le tout modulo 256
// L'étiquette des noeuds internes sera égale à -1
void etiquetteFeuilleNB(noeudBinaire **arbre, int *etiq) {
	if((*arbre)->filsNO == NULL) {
		// On place la valeur de l'étiquette dans la feuille
		(*arbre)->etiq = *etiq % 256;
		(*etiq) += 20;
	} else {
		// On place la valeur -1 dans la feuille puisqu'il s'agit d'un noeud
		// et on réitère le processus pour chaque fils
		etiquetteFeuilleNB(&((*arbre)->filsNO), etiq);
		etiquetteFeuilleNB(&((*arbre)->filsNE), etiq);
		etiquetteFeuilleNB(&((*arbre)->filsSO), etiq);
		etiquetteFeuilleNB(&((*arbre)->filsSE), etiq);
		(*arbre)->etiq = -1;
	}
}

// Affichage de l'arbre dans la console pour vérifier son contenu
void afficheArbre(noeudBinaire *arbre) {
	if(arbre != NULL) {
		printf("Valeur : %d - Etiquette : %d - Coordonnées : x(%d;%d) y(%d;%d)\n", arbre->valeur, arbre->etiq, arbre->xmin, arbre->xmax, arbre->ymin, arbre->ymax);
		afficheArbre(arbre->filsNO);
		afficheArbre(arbre->filsNE);
		afficheArbre(arbre->filsSO);
		afficheArbre(arbre->filsSE);
	}
}

// Créer une matrice correspondant aux valeurs des feuilles (0 pour noir, 255 pour blanc)
// ou aux étiquettes d'un arbre quaternaire
void creeMatriceArbreNB(noeudBinaire *arbre, short int ***resultatBinaire, int etiq) {
	if(arbre->filsNO == NULL) {
		if(etiq == 1) {
			// Pour une étiquette vrai, la matrice resultat contient la valeur des étiquettes
			for(int i = arbre->ymin; i < arbre->ymax; i++)
				for(int j = arbre->xmin; j < arbre->xmax; j++)
					(*resultatBinaire)[i][j] = arbre->etiq;
		} else {
			// Dans le cas inverse, elle contient la valeur des feuilles (blanc ou noir)
			for(int i = arbre->ymin; i < arbre->ymax; i++)
				for(int j = arbre->xmin; j < arbre->xmax; j++)
					(*resultatBinaire)[i][j] = arbre->valeur * 255;
		}
	} else {
		// On réitère le processus pour chaque fils lorsque nous sommes dans un noeud
		creeMatriceArbreNB(arbre->filsNO, resultatBinaire, etiq);
		creeMatriceArbreNB(arbre->filsNE, resultatBinaire, etiq);
		creeMatriceArbreNB(arbre->filsSO, resultatBinaire, etiq);
		creeMatriceArbreNB(arbre->filsSE, resultatBinaire, etiq);
	}
}

// Libère proprement l'arbre (noeud et feuille) créé par creeArbreNB
void libereArbreNB(noeudBinaire **arbre) {
	// Désallocation des fils filsNO, filsNE, filsSO et filsSE avec affectation de la valeur NULL
	if(*arbre != NULL) {
		libereArbreNB(&((*arbre)->filsNO));
		libereArbreNB(&((*arbre)->filsNE));
		libereArbreNB(&((*arbre)->filsSO));
		libereArbreNB(&((*arbre)->filsSE));
		
		free(*arbre);
		(*arbre) = NULL;
	}
}
