typedef struct tabArbreBinaire {
    coordonnees coord;
    int valeur, etiq;
} tabArbreBinaire;

void nbEtiquette(noeudBinaire *arbre, int *compteur);

void rempliTab(noeudBinaire *arbre, tabArbreBinaire **tab, int *compteur);

void upTab(tabArbreBinaire **tab, int oldEtiq, int newEtiq, int nbDonnees);

void optimiseArbreBinaire(noeudBinaire **arbre, tabArbreBinaire **tab, int nbDonnees);

void fusionNB(noeudBinaire **arbre);

void decaleTab(tabArbreBinaire **tabDonneesOpt, int depart, int compteur);

void tabOpt(tabArbreBinaire *tabDonnees, tabArbreBinaire **tabDonneesOpt, int *nbDonnees);

void sauveFeuilleBinaireOpt(noeudBinaire *noeud, FILE *f);

void compressionBinaireOptSP(noeudBinaire *arbre);

void sauveMatriceBinaireOpt(short int ***recupImage, FILE *f);

void decompressionBinaireOptSP(short int ***recupImage);

void creeMatriceQK(int ***matriceQK, int k, int largeur, int hauteur);

void quantifieMatrice(int ***matriceQuant, int **matriceQK, short int **image, int largeur, int hauteur);

void dequantifieMatrice(short int ***matriceDequant, int **matriceQK, short int **matriceQuant, int largeur, int hauteur);

void calculErreur(short int **matriceDequant, short int **image, int largeur, int hauteur);

void parcourtZigzagInt(int **matriceQuant, short int **tabZigzag, int i, int j, int indice, int taille);

void sauveFeuilleNG_AP_K(int **matriceQuant, FILE *f, int factQualite, short int hauteur, short int largeur);

void compressionNG_AP_K(arbreImage **arbreHaar, int factQualite, short int **recupImageNGOpt);

void sauveMatriceNG_AP_K(short int ***recupImage, FILE *f, int taille);

void decompressionNG_AP_K(int factQualite, short int ***matriceDequant);

void libereMatriceEntier(int hauteurImage, int ***image2Arbre);