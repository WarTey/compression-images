#include <stdio.h>		// Pour pouvoir lire et écrire dans des fichiers
#include <stdlib.h> 	// Pour pouvoir utiliser exit()
#include <string.h>		// Pour pouvoir utiliser malloc()
#include "BmpLib.h"		// Cet include permet de manipuler des fichiers BMP
#include "structure.h"
#include "compressionBinaire.h"
#include "gereMatrice.h"

// Permet de sauvegarder les données d'un noeud dans un fichier
void sauveFeuilleBinaire(noeudBinaire *noeud, FILE *f) {
	if(noeud->filsNO == NULL) {
		// Détermine la taille de l'image du noeud
		short int taille = noeud->xmax - noeud->xmin;
		// Si la valeur du noeud est 0 (noir), taille devient négatif
		// pour optimiser la place dans le fichier
		if(noeud->valeur == 0)
			taille = -taille;

		// On sauvegarde les coordonnées xmin, ymin et la taille du noeud dans le fichier
		fwrite(&(noeud->xmin), sizeof(short int), 1, f);
		fwrite(&(noeud->ymin), sizeof(short int), 1, f);
		fwrite(&taille, sizeof(short int), 1, f);
	} else {
		// On réitère le processus pour chaque fils
        sauveFeuilleBinaire(noeud->filsNO, f);
        sauveFeuilleBinaire(noeud->filsNE, f);
        sauveFeuilleBinaire(noeud->filsSO, f);
        sauveFeuilleBinaire(noeud->filsSE, f);
    }
}

// Crée le fichier binaire associé à un arbre quaternaire décrivant une image binaire
void compressionBinaireSP(noeudBinaire *noeud) {
	// Ouvre le fichier binaire imageBinaire.bin en écriture
	FILE *f = fopen("imageBinaire.bin", "wb");
	// Permet de prévenir des problèmes de lecture
	if(f == NULL)
		return;
	// Initialise l'en-tête du fichier
    short int nbLig = noeud->ymax, nbCol = noeud->xmax;
    char nbBits = 1, typeCompression = 0;

	// Ecrit l'en-tête dans le fichier binaire
	fwrite(&nbLig, sizeof(short int), 1, f);
    fwrite(&nbCol, sizeof(short int), 1, f);
    fwrite(&nbBits, sizeof(char), 1, f);
    fwrite(&typeCompression, sizeof(char), 1, f);

	// Sauvegarde les données de chaque feuille dans le fichier binaire
	sauveFeuilleBinaire(noeud, f);
	// Ferme le fichier
	fclose(f);
}

// Permet de récupérer les données du fichier dans une matrice
void sauveMatriceBinaire(short int ***recupImage, FILE *f) {
	// Initialise des variables locales qui vont contenir les coordonnées et la taille (couleur)
	// à stocker dans la matrice
	short int xMin = 0, yMin = 0, taille = 0;

	// Procède à une lecture du fichier tant qu'il contient des données
	// Lit les coordonnées et la taille (couleur) contenus dans le fichier
	if(fread(&xMin, sizeof(short int), 1, f)) {
	    fread(&yMin, sizeof(short int), 1, f);
	    fread(&taille, sizeof(short int), 1, f);

		// Détermine et stock la couleur du pixel (à partir de taille) dans la matrice recupImage
		for(int i = yMin; i < yMin + abs(taille); i++) {
			for(int j = xMin; j < xMin + abs(taille); j++) {
				if(taille < 0)
					(*recupImage)[i][j] = 0;
				else
					(*recupImage)[i][j] = 255;
			}
		}
		// Réitère le processus tant qu'il y a des données à récupérer
		sauveMatriceBinaire(recupImage, f);
	}
}

// Crée la matrice correspondant à la description de l'image, contenue dans un fichier binaire
void decompressionBinaireSP(short int ***recupImage) {
	// Ouvre le fichier binaire imageBinaire.bin en lecture
	FILE *f = fopen("imageBinaire.bin", "rb");
	// Permet de prévenir des problèmes de lecture
	if(f == NULL)
		return;
	// Initialise des variables locales qui vont contenir l'en-tête du fichier
	short int nbLig = 0, nbCol = 0;
    char nbBits = 0, typeCompression = 0;

	// Lit l'en-tête du fichier binaire
    fread(&nbLig, sizeof(short int), 1, f);
    fread(&nbCol, sizeof(short int), 1, f);
    fread(&nbBits, sizeof(char), 1, f);
    fread(&typeCompression, sizeof(char), 1, f);

	// Initialise dynamiquement la matrice recupImage
	(*recupImage) = (short int **) malloc(sizeof(short int *) * nbLig);
	for(int i = 0; i < nbLig; i++)
        (*recupImage)[i] = (short int *) malloc(sizeof(short int) * nbCol);

	// Récupère les données du fichier dans la matrice recupImage
	sauveMatriceBinaire(recupImage, f);
	// Ferme le fichier
	fclose(f);
}

// Permet de déterminer le nombre d'optimisations d'un tableau
void placeOptimise(short int *tabZigzag, int taille, int *nbOptimisation) {
	// Contient le nombre d'itérations de chaque valeur
	int temp = 1;
	// On incrémente le compteur nbOptimisation pour chaque valeur égale qui se suive
	for(int i = 0; i < taille * taille - 1; i++) {
		if(tabZigzag[i] != tabZigzag[i+1]) {
			(*nbOptimisation)++;
			temp = 1;
		} else {
			// On incrémente la valeur pour chaque itération de la valeur
			temp++;
			// Si le nombre d'itérations devient plus grand qu'un unsigned short int,
			// on incrémente la variable d'optimisation pour occuper la case d'après dans les tableaux
			if(temp == 65000)
				(*nbOptimisation)++;
		}
	}
	// On fini par incrémenter le nombre d'itérations pour prendre en compte la dernière valeur du tableau
	(*nbOptimisation)++;
}

// Permet d'optimiser un tableau et détermine le nombre d'itérations de chacune de ses valeurs dans un autre tableau
void optimiseTableau(short int **tabZigzagOpti, unsigned short int **tabZigzagIte, short int *tabZigzag, int taille) {
	int nbOptimisation = 0;
	for(int i = 0; i < taille * taille - 1; i++) {
		if(tabZigzag[i] != tabZigzag[i+1]) {
			// On stock la valeur dans tabZigzagOpti puisqu'elle est différente de la suivante
			// puis on incrémente son nombre d'itérations
			(*tabZigzagOpti)[nbOptimisation] = tabZigzag[i];
			(*tabZigzagIte)[nbOptimisation]++;
			// On passe à la case suivante du tableau optimisé
			nbOptimisation++;
		} else {
			// On incrémente le nombre d'itérations de la valeur puisqu'elle est égale à la suivante
			(*tabZigzagIte)[nbOptimisation]++;
			// On vérifie que le nombre d'itérations ne se rapproche pas trop de la limite d'un unsigned short int
			// Dans ce cas, on passe à la case suivante du tableau optimisé
			if((*tabZigzagIte)[nbOptimisation] == 64000)
				nbOptimisation++;
		}
	}

	// On stock la dernière valeur du tableau tout en incrémentant son nombre d'itérations
	(*tabZigzagOpti)[nbOptimisation] = tabZigzag[taille * taille - 1];
	(*tabZigzagIte)[nbOptimisation]++;
}

// Permet de sauvegarder les données d'un noeud dans un fichier
void sauveFeuilleNG(arbreImage *noeud, FILE *f) {
	if(noeud->filsNO == NULL) {
		// Détermine la largeur et la hauteur de l'image du noeud
		short int tailleLargeur = noeud->xmax - noeud->xmin, tailleHauteur = noeud->ymax - noeud->ymin;

		// On sauvegarde les coordonnées xmin et ymin et la hauteur du noeud dans le fichier
		fwrite(&(noeud->xmin), sizeof(short int), 1, f);
		fwrite(&(noeud->ymin), sizeof(short int), 1, f);
		fwrite(&tailleHauteur, sizeof(short int), 1, f);
		
		// Initialise dynamiquement un tableau contenant les résultats du parcourt en zigzag
		short int *tabZigzag = (short int *) malloc(sizeof(short int) * tailleLargeur * tailleHauteur);
		// Permet de transformer la matrice imageDivisee en un tableau à l'aide d'un parcourt en zigzag
		parcourtZigzag(noeud->imageDivisee, &tabZigzag, 0, tailleHauteur - 1, 0, tailleHauteur);

		// Permet de stocker le nombre d'optimisations
		int nbOptimisation = 0;
		// Détermine le nombre d'optimsations de tabZigzag
		placeOptimise(tabZigzag, tailleHauteur, &nbOptimisation);
		// Ecrit le nombre d'optimisations dans le fichier
		fwrite(&nbOptimisation, sizeof(int), 1, f);
		
		// Initialise dynamiquement deux tableau dont un contenant le parcourt en zigzag optimisé
		// et l'autre le nombre d'itérations de chaque valeur du tableau précédent
		short int *tabZigzagOpti = (short int *) malloc(sizeof(short int) * nbOptimisation);
		unsigned short int *tabZigzagIte = (unsigned short int *) malloc(sizeof(unsigned short int) * nbOptimisation);

		// Initialise le tableau d'itérations pour éviter d'avoir des valeurs différents de 0 au départ
		for(int i = 0; i < nbOptimisation; i++)
			tabZigzagIte[i] = 0;

		// Optimise le tableau en zigzag et détermine le nombre d'itérations de chaque valeur
		optimiseTableau(&tabZigzagOpti, &tabZigzagIte, tabZigzag, tailleHauteur);
		// Ecrit les tableaux optimisés dans le fichier
		fwrite(tabZigzagOpti, sizeof(short int), nbOptimisation, f);
		fwrite(tabZigzagIte, sizeof(unsigned short int), nbOptimisation, f);

		// Libère proprement les tableaux créés précédemment
		libereTableauZigzag(&tabZigzag, &tabZigzagOpti, &tabZigzagIte);
	} else {
		// Réitère le processus pour chaque fils
        sauveFeuilleNG(noeud->filsNO, f);
        sauveFeuilleNG(noeud->filsNE, f);
        sauveFeuilleNG(noeud->filsSO, f);
        sauveFeuilleNG(noeud->filsSE, f);
    }
}

// Crée le fichier binaire associé à un arbre quaternaire décrivant une image en niveaux de gris
void compressionNG_SP(arbreImage *noeud) {
	// Ouvre le fichier binaire imageNG.bin en écriture
	FILE *f = fopen("imageNG.bin", "wb");
	// Permet de prévenir des problèmes de lecture
	if(f == NULL)
		return;
	// Initialise l'en-tête du fichier
	short int nbLig = noeud->ymax - noeud->ymin, nbCol = noeud->xmax - noeud->xmin;
    char nbBits = 8, typeCompression = 0;

	// Ecrit l'en-tête dans le fichier binaire
	fwrite(&nbLig, sizeof(short int), 1, f);
    fwrite(&nbCol, sizeof(short int), 1, f);
    fwrite(&nbBits, sizeof(char), 1, f);
    fwrite(&typeCompression, sizeof(char), 1, f);
	
	// Sauvegarde les données de chaque feuille dans le fichier binaire
	sauveFeuilleNG(noeud, f);
	// Ferme le fichier
	fclose(f);
}

// Permet de transformer une matrice en un tableau à l'aide d'un parcourt en zigzag
void parcourtZigzag(short int **imageDivisee, short int **tabZigzag, int i, int j, int indice, int taille) {
	int compteur = 0;
	// On parcourt en zigzag chaque pixel de la matrice
	while(j > -1 && j < taille && i > -1 && i < taille) {
		// Stock la valeur de la matrice dans le tableau
		(*tabZigzag)[compteur] = imageDivisee[j][i];
		compteur++;
		// Une fois en haut ou en bas de la matrice, on se décale de 1 pixel pour continuer le parcourt
		if((j == taille - 1 && indice == 0) || (j == 0 && indice == 1)) {
			i += 1;
			indice = (indice + 1) % 2;
		// Une fois à gauche ou à droite de la matrice, on se décale de 1 pixel pour continuer le parcourt
		} else if((i == taille - 1 && indice == 0) || (i == 0 && indice == 1)) {
			j -= 1;
			indice = (indice + 1) % 2;
		// Dans le cas restant, on monte ou on descend en diagonale
		} else
			(indice == 0) ? (i++, j++) : (i--, j--);
	}
}

// Recompose une matrice à partir d'un tableau contenant les valeurs du parcourt en zigzag
void recompositionZigzag(short int ***imageDivisee, short int *tabZigzag, int i, int j, int indice, int taille) {
	int compteur = 0;
	// On parcourt en zigzag chaque pixel de la matrice
	while(j > -1 && j < taille && i > -1 && i < taille) {
		// Stock la valeur du tableau dans la matrice
		(*imageDivisee)[j][i] = tabZigzag[compteur];
		compteur++;
		// Une fois en haut ou en bas de la matrice, on se décale de 1 pixel pour continuer le parcourt
		if((j == taille - 1 && indice == 0) || (j == 0 && indice == 1)) {
			i += 1;
			indice = (indice + 1) % 2;
		// Une fois à gauche ou à droite de la matrice, on se décale de 1 pixel pour continuer le parcourt
		} else if((i == taille - 1 && indice == 0) || (i == 0 && indice == 1)) {
			j -= 1;
			indice = (indice + 1) % 2;
		// Dans le cas restant, on monte ou on descend en diagonale
		} else
			(indice == 0) ? (i++, j++) : (i--, j--);
	}
}

// Copie les valeurs d'un tableau optimisé vers un autre tableau non-optimisé
void deOptimiseTableau(short int **tabZigzag, short int *tabZigzagOpti, unsigned short int *tabZigzagIte, int taille) {
	int compteur = 0;
	for(int i = 0; i < taille; i++) {
		for (int k = 0; k < tabZigzagIte[i]; k++) {
			// On stock la valeur dans le tableau non-optimisé pour chaque itération
			(*tabZigzag)[compteur] = tabZigzagOpti[i];
			compteur++;
		}
	}
}

void sauveMatriceNG(short int ***recupImage, FILE *f) {
	// Initialise des variables locales qui vont contenir les coordonnées, la taille 
	// et le nombre d'optimisations à stocker dans la matrice
	short int xMin = 0, yMin = 0, taille = 0;
	int nbOptimisation = 0;

	// Procède à une lecture du fichier tant qu'il contient des données
	// Lit les coordonnées, la taille et le nombre d'optimisations contenus dans le fichier
	if(fread(&xMin, sizeof(short int), 1, f)) {
	    fread(&yMin, sizeof(short int), 1, f);
	    fread(&taille, sizeof(short int), 1, f);
		fread(&nbOptimisation, sizeof(int), 1, f);

		// Initialise dynamiquement deux tableau dont un contenant le parcourt en zigzag optimisé
		// et l'autre le nombre d'itérations de chaque valeur du tableau précédent
		short int *tabZigzagOpti = (short int *)malloc(sizeof(short int) * nbOptimisation);
		unsigned short int *tabZigzagIte = (unsigned short int *)malloc(sizeof(unsigned short int) * nbOptimisation);
		// Complète les deux tableaux précédent à l'aide du fichier
		fread(tabZigzagOpti, sizeof(short int), nbOptimisation, f);
		fread(tabZigzagIte, sizeof(unsigned short int), nbOptimisation, f);

		// Initialise dynamiquement un tableau contenant les valeurs du parcourt en zigzag non-optimisé
		short int *tabZigzag = (short int *) malloc(sizeof(short int) * taille * taille);
		// Copie les données du tableau en zigzag optimisé dans celui non-optimisé
		deOptimiseTableau(&tabZigzag, tabZigzagOpti, tabZigzagIte, nbOptimisation);

		// Initialise dynamiquement la matrice recupImageFils
		short int **recupImageFils = (short int **) malloc(sizeof(short int *) * taille);
		for(int i = 0; i < taille; i++)
	        recupImageFils[i] = (short int *) malloc(sizeof(short int) * taille);

		// Recompose une matrice à partir du tableau contenant les valeurs du parcourt en zigzag
		recompositionZigzag(&recupImageFils, tabZigzag, 0, taille - 1, 0, taille);
		// Copie la matrice précédente dans la matrice finale recupImage
		for(int i = 0; i < taille; i++)
			for(int j = 0; j < taille; j++)
				(*recupImage)[i + yMin][j + xMin] = recupImageFils[i][j];

		// Libère proprement la matrice recupImageFils créée précédemment
		libereMatrice(taille, &recupImageFils);
		// Libère proprement les tableaux tabZigzag, tabZigzagOpti et tabZigzagIte créées précédemment
		libereTableauZigzag(&tabZigzag, &tabZigzagOpti, &tabZigzagIte);

		// Rétière le processus tant que le fichier contient des données
		sauveMatriceNG(recupImage, f);
	}
}

// Crée la matrice correspondant à la description de l'image, contenue dans un fichier en niveaux de gris
void decompressionNG_SP(short int ***recupImage) {
	// Ouvre le fichier binaire imageNG.bin en lecture
	FILE *f = fopen("imageNG.bin", "rb");
	// Permet de prévenir des problèmes de lecture
	if(f == NULL)
		return;
	// Initialise des variables locales qui vont contenir l'en-tête du fichier
	short int nbLig = 0, nbCol = 0;
    char nbBits = 0, typeCompression = 0;

	// Lit l'en-tête du fichier binaire
	fread(&nbLig, sizeof(short int), 1, f);
    fread(&nbCol, sizeof(short int), 1, f);
    fread(&nbBits, sizeof(char), 1, f);
    fread(&typeCompression, sizeof(char), 1, f);

	// Initialise dynamiquement la matrice recupImage
	(*recupImage) = (short int **) malloc(sizeof(short int *) * nbLig);
	for(int i = 0; i < nbLig; i++)
        (*recupImage)[i] = (short int *) malloc(sizeof(short int) * nbCol);

	// Récupère les données du fichier dans la matrice recupImage
	sauveMatriceNG(recupImage, f);
	// Ferme le fichier
	fclose(f);
}

// Libère proprement les tableaux créés par sauveFeuilleNG
void libereTableauZigzag(short int **tabZigzag, short int **tabZigzagOpti, unsigned short int **tabZigzagIte) {
	// Désallocation des tableaux tabZigzag, tabZigzagOpti et tabZigzagIte avec affectation de la valeur NULL
	free(*tabZigzag);
	free(*tabZigzagOpti);
	free(*tabZigzagIte);
	(*tabZigzag) = NULL;
	(*tabZigzagOpti) = NULL;
	(*tabZigzagIte) = NULL;
}
