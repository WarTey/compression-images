void negatifImage(short int **rouge, short int **vert, short int **bleu, short int ***rougeInverse, short int ***vertInverse, short int ***bleuInverse, int hauteurImage, int largeurImage);

void couleur2NG(short int **rouge, short int **vert, short int **bleu, short int ***niveauGris, int hauteurImage, int largeurImage);

void seuillageNG(short int **niveauGris, short int ***seuillage, int seuil, int hauteurImage, int largeurImage);

int triTab(int *tab, int taille);

void filtreMedian3x3(short int **niveauGris, short int ***filtreMedian, int hauteurImage, int largeurImage);
