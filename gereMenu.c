#include <stdlib.h>     // Pour pouvoir utiliser exit()
#include <stdio.h>      // Pour pouvoir utiliser printf()
#include <string.h>     // Pour allouer dynamiquement
#include "GfxLib.h"     // Seul cet include est necessaire pour faire du graphique
#include "BmpLib.h"     // Cet include permet de manipuler des fichiers BMP
#include "structure.h"
#include "gereMatrice.h"
#include "traitement.h"
#include "gereMenu.h"
#include "arbreQuadtree.h"
#include "arbreHaar.h"
#include "compressionBinaire.h"
#include "fusionQuantification.h"

void initImage(nomImage *imageInfo) {
    // Initialise les messages d'erreurs
    // Copie la chaîne de caractères dans imageInfo->erreurDimension
    strcpy(imageInfo->erreurDimension, "Image de dimension incorrecte.");
	strcpy(imageInfo->erreurLecture, "Nom de l'image incorrect. (./images/Default.bmp)");
    // Initialise la taille de la police
	imageInfo->tailleErreur = 24;

    // Initialise le nom des images à traiter/sauvegarder
    strcpy(imageInfo->nomImage[0], "./images/Default.bmp");
    strcpy(imageInfo->nomImage[1], "./images/Copie.bmp");
    strcpy(imageInfo->nomImage[2], "./images/Negatif.bmp");
	strcpy(imageInfo->nomImage[3], "./images/Gris.bmp");
	strcpy(imageInfo->nomImage[4], "./images/Seuil.bmp");
	strcpy(imageInfo->nomImage[5], "./images/Median.bmp");
    strcpy(imageInfo->nomImage[6], "./images/ArbreBinaire.bmp");
    strcpy(imageInfo->nomImage[7], "./images/CompressionHaar.bmp");
    strcpy(imageInfo->nomImage[8], "./images/BinaireSansPerte.bmp");
    strcpy(imageInfo->nomImage[9], "./images/NGSansPerte.bmp");
    strcpy(imageInfo->nomImage[10], "./images/BinaireAvecPerte.bmp");
    strcpy(imageInfo->nomImage[11], "./images/NGAvecPerte.bmp");
}

void initImageTemoin(DonneesImageRGB **imageTemoin, DonneesImageRGB *image) {
    // On libère la variable imageTemoin si celle-ci contient des données pour éviter de faire de allocation excessive
    if((*imageTemoin) != NULL)
        libereDonneesImageRGB(imageTemoin);

    if(image != NULL) {
        // Initialise dynamiquement la variable image ainsi que le tableau contenant les données de l'image
        (*imageTemoin) = (DonneesImageRGB *) malloc(sizeof(DonneesImageRGB));
        (*imageTemoin)->donneesRGB = (unsigned char *) malloc(sizeof(unsigned char) * image->hauteurImage * image->largeurImage * 3);

        // Copie la largeur et hauteur en paramètres dans la variable image
        (*imageTemoin)->largeurImage = image->largeurImage;
        (*imageTemoin)->hauteurImage = image->hauteurImage;

        // Copie les valeurs des trois matrices rouge, vert et bleu vers donneesRGB
        for(int i = 0; i < image->hauteurImage * image->largeurImage * 3; i++)
            (*imageTemoin)->donneesRGB[i] = image->donneesRGB[i];
    }
}

void initBouton(bouton *boutonInfos) {
    // Indicateur permettant de déterminer dans quel menu l'utilisateur se trouve
    boutonInfos->index = 0;
    // Initialise la taille du texte à l'intérieur des boutons
    boutonInfos->tailleTexte = 18;

    // Initialise le texte des boutons
	strcpy(boutonInfos->couleur[0], "Couleur");
    strcpy(boutonInfos->couleur[1], "Negatif");
    strcpy(boutonInfos->couleur[2], "Gris");
	strcpy(boutonInfos->couleur[3], "Seuil");
	strcpy(boutonInfos->couleur[4], "Median");
	strcpy(boutonInfos->couleur[5], "Save");
    strcpy(boutonInfos->couleur[6], "+");
    strcpy(boutonInfos->couleur[7], "-");
    strcpy(boutonInfos->couleur[8], "Decoupe Binaire");
    strcpy(boutonInfos->couleur[9], "Compression Haar");
    strcpy(boutonInfos->couleur[10], "Sans Perte");
    strcpy(boutonInfos->couleur[11], "Avec Perte");
}

void initMatrices(matImage **imageModif, DonneesImageRGB **image, chaine nomImage) {
    // Initialise les données de l'image dans la structure DonneesImageRGB
    (*image) = lisBMPRGB(nomImage);

    // Effectue les opérations qui suivent si l'image à était lu correctement
    if((*image) != NULL) {
        // Initialise dynamiquement la structure matImage
        (*imageModif) = (matImage *) malloc(sizeof(matImage));
        // Copie les dimensions de l'image dans la structure matImage
        (*imageModif)->largeurImage = (*image)->largeurImage;
        (*imageModif)->hauteurImage = (*image)->hauteurImage;
        // Affecte un seuillage par défaut que l'utilisateur pourra changer sur l'interface
        (*imageModif)->seuil = 125;
        (*imageModif)->seuillage = NULL;

        // Initialise chaque matrice de la structe matImage
        cree3Matrices((*image), &((*imageModif)->rouge), &((*imageModif)->vert), &((*imageModif)->bleu));
        negatifImage((*imageModif)->rouge, (*imageModif)->vert, (*imageModif)->bleu, &((*imageModif)->rougeInverse), &((*imageModif)->vertInverse), &((*imageModif)->bleuInverse), (*imageModif)->hauteurImage, (*imageModif)->largeurImage);
        couleur2NG((*imageModif)->rouge, (*imageModif)->vert, (*imageModif)->bleu, &((*imageModif)->niveauGris), (*imageModif)->hauteurImage, (*imageModif)->largeurImage);
        seuillageNG((*imageModif)->niveauGris, &((*imageModif)->seuillage), (*imageModif)->seuil, (*imageModif)->hauteurImage, (*imageModif)->largeurImage);
        filtreMedian3x3((*imageModif)->niveauGris, &((*imageModif)->filtreMedian), (*imageModif)->hauteurImage, (*imageModif)->largeurImage);
        creeMatrice((*image), &((*imageModif)->resultatBinaire));
    }
}

void initArbre(noeudBinaire **arbre, matImage *imageModif) {
    // Créer l'arbre quaternaire à partir de la matrice seuillage
    creeArbreNB(imageModif->seuillage, arbre, 0, 0, imageModif->largeurImage, imageModif->hauteurImage);
    // Atttribue à chacune des feuille un numéro (étiquette)
    etiquetteFeuilleNB(arbre, &((*arbre)->etiq));
}

void creeArbreBinaire(matImage *imageModif, noeudBinaire **arbre) {
    // Initialise l'arbre binaire correspondant à la matrice seuillage actuelle
    libereArbreNB(arbre);
    initArbre(arbre, imageModif);
    // Créer une matrice correspondant à l'arbre binaire
    traitementBinaire(&imageModif, arbre);
}

void creeArbreHaar(DonneesImageRGB **image, matImage *imageModif, arbreImage **arbreHaar) {
    // Demande un niveau de décomposition à l'utilisateur
    int niveauDecomposition = niveauDecompositionHaar();
    // Crée l'arbre complet correspondant à l'image en niveaux de gris
    decomposeNniveaux(imageModif, arbreHaar, niveauDecomposition);
    // Demande un niveau de reconstitution à l'utilisateur
    int niveauReconstitution = niveauReconstitutionHaar(niveauDecomposition);
    // Sauvegarde l'arbre selon niveauReconstitution
    sauveArbreImage(image, arbreHaar, niveauReconstitution, 0);
    // Libère proprement l'arbre complet
    libereArbre(arbreHaar);
}

void libereMatrices(matImage **imageModif, DonneesImageRGB **image, DonneesImageRGB **imageTemoin, noeudBinaire **arbre) {
    // Libère proprement l'arbre binaire
    libereArbreNB(arbre);

    // Libère proprement les matrices de la structure matImage
    libereMatrice((*imageModif)->hauteurImage, &((*imageModif)->resultatBinaire));
    libere3Matrices((*imageModif)->hauteurImage, &((*imageModif)->filtreMedian), &((*imageModif)->seuillage), &((*imageModif)->niveauGris));
    libere3Matrices((*imageModif)->hauteurImage, &((*imageModif)->rougeInverse), &((*imageModif)->vertInverse), &((*imageModif)->bleuInverse));
    libere3Matrices((*imageModif)->hauteurImage, &((*imageModif)->rouge), &((*imageModif)->vert), &((*imageModif)->bleu));
    free(*imageModif);
    (*imageModif) = NULL;

    // Libère les données des images qui s'affichent à l'écran
    libereDonneesImageRGB(image);
    libereDonneesImageRGB(imageTemoin);
}

void traitementBinaire(matImage **imageModif, noeudBinaire **arbre) {
    // Copie le contenu de arbre dans un noeudBinaire temporaire
    noeudBinaire *arbreTemp = *arbre;

    // Affiche l'arbre binaire en console
    if(AFF_ARBRE_BIN == 1) {
        printf("Affichage de l'arbre binaire : \n\n");
        afficheArbre(arbreTemp);
        printf("\n");
    }

    // Créer une matrice correspondant aux valeurs des feuilles/étiquettes
    creeMatriceArbreNB(arbreTemp, &((*imageModif)->resultatBinaire), SAVE_ETIQ);
}

void affichage(DonneesImageRGB *image, DonneesImageRGB *imageTemoin, nomImage imageInfo, bouton boutonInfos, int erreurDimension) {
    // Définit une couleur courante rouge et une épaisseur de trait de taille 3
    couleurCourante(255, 0, 0);
    epaisseurDeTrait(3);

    // Effectue les opérations qui suivent si l'image à était lu correctement
    if(image != NULL) {
        // On vérifie qu'il s'agisse bien d'une image de dimension (2^N x 2^N)
        if(image->largeurImage == image->hauteurImage && erreurDimension == 1) {
            // Affiche l'image de droite sur l'interface graphique
            ecrisImage(5 * (largeurFenetre() - image->largeurImage) / 6, (hauteurFenetre() - image->hauteurImage) / 2, image->largeurImage, image->hauteurImage, image->donneesRGB);
            // Affiche l'image de gauche sur l'interface graphique (image de comparaison)
            ecrisImage((largeurFenetre() - imageTemoin->largeurImage) / 6, (hauteurFenetre() - imageTemoin->hauteurImage) / 2, imageTemoin->largeurImage, imageTemoin->hauteurImage, imageTemoin->donneesRGB);
            
            // Définit une couleur courante orange
            couleurCourante(210, 128, 0);
            // Dessine les boutons sur l'interface graphique
            rectangle(largeurFenetre() / 16, 0, 3 * largeurFenetre() / 16, hauteurFenetre() / 16);
            rectangle(4 * largeurFenetre() / 16, 0, 6 * largeurFenetre() / 16, hauteurFenetre() / 16);
            rectangle(7 * largeurFenetre() / 16, 0, 9 * largeurFenetre() / 16, hauteurFenetre() / 16);
            rectangle(10 * largeurFenetre() / 16, 0, 12 * largeurFenetre() / 16, hauteurFenetre() / 16);
            rectangle(13 * largeurFenetre() / 16, 0, 15 * largeurFenetre() / 16, hauteurFenetre() / 16);
            rectangle(7 * largeurFenetre() / 16, 15 * hauteurFenetre() / 16, 9 * largeurFenetre() / 16, hauteurFenetre());
            rectangle(largeurFenetre() / 16, 15 * hauteurFenetre() / 16, 3 * largeurFenetre() / 16, hauteurFenetre());
            rectangle(4 * largeurFenetre() / 16, 15 * hauteurFenetre() / 16, 6 * largeurFenetre() / 16, hauteurFenetre());
            rectangle(10 * largeurFenetre() / 16, 15 * hauteurFenetre() / 16, 12 * largeurFenetre() / 16, hauteurFenetre());
            rectangle(13 * largeurFenetre() / 16, 15 * hauteurFenetre() / 16, 15 * largeurFenetre() / 16, hauteurFenetre());
            if (boutonInfos.index == 4) {
                rectangle(0, 15 * hauteurFenetre() / 32, 3*largeurFenetre() / 32, 17 * hauteurFenetre() / 32);
                rectangle(29 * largeurFenetre() / 32, 15 * hauteurFenetre() / 32, largeurFenetre(), 17 * hauteurFenetre() / 32);
            }

            // Définit une couleur courante noir et une épaisseur de trait de taille 2
            couleurCourante(0, 0, 0);
            epaisseurDeTrait(2);
            // Affiche la chaîne de caractères à l'intérieur de chaque bouton
            afficheChaine(boutonInfos.couleur[0], boutonInfos.tailleTexte, 2 * largeurFenetre() / 16 - tailleChaine(boutonInfos.couleur[0], boutonInfos.tailleTexte) / 2, hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            afficheChaine(boutonInfos.couleur[1], boutonInfos.tailleTexte, 5 * largeurFenetre() / 16 - tailleChaine(boutonInfos.couleur[1], boutonInfos.tailleTexte) / 2, hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            afficheChaine(boutonInfos.couleur[2], boutonInfos.tailleTexte, 8 * largeurFenetre() / 16 - tailleChaine(boutonInfos.couleur[2], boutonInfos.tailleTexte) / 2, hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            afficheChaine(boutonInfos.couleur[3], boutonInfos.tailleTexte, 11 * largeurFenetre() / 16 - tailleChaine(boutonInfos.couleur[3], boutonInfos.tailleTexte) / 2, hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            afficheChaine(boutonInfos.couleur[4], boutonInfos.tailleTexte, 14 * largeurFenetre() / 16 - tailleChaine(boutonInfos.couleur[4], boutonInfos.tailleTexte) / 2, hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            afficheChaine(boutonInfos.couleur[5], boutonInfos.tailleTexte, 8 * largeurFenetre() / 16 - tailleChaine(boutonInfos.couleur[5], boutonInfos.tailleTexte) / 2, 31 * hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            afficheChaine(boutonInfos.couleur[8], boutonInfos.tailleTexte, 2 * largeurFenetre() / 16 - tailleChaine(boutonInfos.couleur[8], boutonInfos.tailleTexte) / 2, 31 * hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            afficheChaine(boutonInfos.couleur[9], boutonInfos.tailleTexte, 5 * largeurFenetre() / 16 - tailleChaine(boutonInfos.couleur[9], boutonInfos.tailleTexte) / 2, 31 * hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            afficheChaine(boutonInfos.couleur[10], boutonInfos.tailleTexte, 11 * largeurFenetre() / 16 - tailleChaine(boutonInfos.couleur[10], boutonInfos.tailleTexte) / 2, 31 * hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            afficheChaine(boutonInfos.couleur[11], boutonInfos.tailleTexte, 14 * largeurFenetre() / 16 - tailleChaine(boutonInfos.couleur[11], boutonInfos.tailleTexte) / 2, 31 * hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            if (boutonInfos.index == 4) {
                afficheChaine(boutonInfos.couleur[7], boutonInfos.tailleTexte, 3 * largeurFenetre() / 64 - tailleChaine(boutonInfos.couleur[7], boutonInfos.tailleTexte) / 2, 16 * hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
                afficheChaine(boutonInfos.couleur[6], boutonInfos.tailleTexte, 61 * largeurFenetre() / 64 - tailleChaine(boutonInfos.couleur[6], boutonInfos.tailleTexte) / 2, 16 * hauteurFenetre() / 32 - boutonInfos.tailleTexte / 2);
            }
        } else {
            // Affiche un message d'erreur sur une erreur de dimension de l'image
            afficheChaine(imageInfo.erreurDimension, imageInfo.tailleErreur, largeurFenetre() / 2 - tailleChaine(imageInfo.erreurDimension, imageInfo.tailleErreur) / 2, hauteurFenetre() / 2 - imageInfo.tailleErreur / 2);
        }
    } else {
        // Affiche un message d'erreur sur une erreur de lecture de l'image
        afficheChaine(imageInfo.erreurLecture, imageInfo.tailleErreur, largeurFenetre() / 2 - tailleChaine(imageInfo.erreurLecture, imageInfo.tailleErreur) / 2, hauteurFenetre() / 2 - imageInfo.tailleErreur / 2);
    }
}

// Renvoie un niveau de décomposition par saisie dans le terminal
int niveauDecompositionHaar() {
    // Contient le niveau de décomposition saisi par l'utilisateur
    int niveauDecomposition = 0;

    // Demande le niveau de décomposititon à l'utilisateur
    printf("Image de Haar\n\n");
    printf("Saisir un niveau de décomposition : ");
    scanf("%d", &niveauDecomposition);
    printf("Niveau de décomposition : %d\n\n", niveauDecomposition);

    // Permet de fonctionner comme un compteur dans les fonctions qui utiliseront cette variable
    if(niveauDecomposition > 0)
        niveauDecomposition += 1;

    // Renvoie le niveau de décomposition
    return niveauDecomposition;
}

// Renvoie un facteur de qualité par saisie dans le terminal
int facteurQualite() {
    // Contient facteur de qualité saisi par l'utilisateur
    int factQualite = 0;

    // Demande le facteur de qualité à l'utilisateur
    printf("Quantification\n\n");
    do {
        printf("Saisir un facteur de qualité entre 1 et 25 : ");
        scanf("%d", &factQualite);
    } while(factQualite < 1 && factQualite > 25);
    printf("Facteur de Qualité : %d\n\n", factQualite);

    // Renvoie le facteur de qualité
    return factQualite;
}

// Renvoie un niveau de reconstitution par saisie dans le terminal
int niveauReconstitutionHaar(int niveauDecomposition) {
    // Contient le niveau de reconstitution saisi par l'utilisateur
    int niveauReconstitution = 0;

    // Demande le niveau de reconstitution à l'utilisateur
    printf("Saisir un niveau de reconstitution : ");
    scanf("%d", &niveauReconstitution);
    printf("Niveau de reconstitution : %d\n\n", niveauReconstitution);

    // Permet de fonctionner comme un compteur dans les fonctions qui utiliseront cette variable
    if (niveauReconstitution > niveauDecomposition - 1 && niveauReconstitution != 0)
        niveauReconstitution = niveauDecomposition - 1;

    // Renvoie le niveau de reconstitution
    return niveauReconstitution;
}

void compressionDecompressionBinaireSP(DonneesImageRGB **image, matImage *imageModif, int *index, noeudBinaire **arbre, arbreImage **arbreHaar) {
    if((*index) != 8) {
        (*index) = 8;
        // Crée l'arbre binaire correspondant à la matrice seuillage
        creeArbreBinaire(imageModif, arbre);
        noeudBinaire *arbreTemp = (*arbre);
        // Crée le fichier binaire associé à l'arbre arbreTemp
        compressionBinaireSP(arbreTemp);
        short int **recupImageBinaire;
        // Crée la matrice correspondant au contenu du fichier binaire
        decompressionBinaireSP(&recupImageBinaire);
        // Copie le contenu de la matrice recupImageBinaire dans DonneesImageRGB
        creeImage(image, recupImageBinaire, recupImageBinaire, recupImageBinaire, (*arbre)->ymax, (*arbre)->xmax);
        // Libère proprement la matrice recupImageBinaire créée précédemment
        libereMatrice((*arbre)->ymax, &recupImageBinaire);
    } else {
        (*index) = 9;
        // Demande un niveau de décomposition à l'utilisateur
        int niveauDecomposition = niveauDecompositionHaar();
        // Crée l'arbre complet correspondant à l'image en niveaux de gris
        decomposeNniveaux(imageModif, arbreHaar, niveauDecomposition);
        arbreImage *arbreHaarTemp = (*arbreHaar);
        // Crée le fichier binaire associé à l'arbre arbreHaarTemp
        compressionNG_SP(arbreHaarTemp);
        short int **recupImageNG;
        // Crée la matrice correspondant au contenu du fichier binaire
        decompressionNG_SP(&recupImageNG);
        // Copie le contenu de la matrice recupImageNG dans DonneesImageRGB
        creeImage(image, recupImageNG, recupImageNG, recupImageNG, (*arbreHaar)->ymax, (*arbreHaar)->xmax);
        // Libère proprement la matrice recupImageNG créée précédemment
        libereMatrice((*arbreHaar)->ymax, &recupImageNG);
        // Libère proprement l'arbre complet
        libereArbre(arbreHaar);
    }
}

void optimisationCompressionDecompression(DonneesImageRGB **image, matImage *imageModif, int *index, noeudBinaire **arbre, arbreImage **arbreHaar) {
    if((*index) == 10) {
        (*index) = 11;
        // Demande un niveau de décomposition à l'utilisateur
        int niveauDecomposition = niveauDecompositionHaar();
        // Crée l'arbre complet correspondant à l'image en niveaux de gris
        decomposeNniveaux(imageModif, arbreHaar, niveauDecomposition);
        arbreImage *arbreHaarTemp = (*arbreHaar);
        // Crée le fichier binaire associé à l'arbre arbreHaarTemp
        compressionNG_SP(arbreHaarTemp);
        short int **recupImageNGOpt;
        // Crée la matrice correspondant au contenu du fichier binaire
        decompressionNG_SP(&recupImageNGOpt);
        // Demande un facteur de qualité à l'utilisateur
        int factQualite = facteurQualite();
        // Crée le fichier binaire de la matrice quantifiée
        compressionNG_AP_K(arbreHaar, factQualite, recupImageNGOpt);
        short int **matriceDequant;
        // Décompresse le fichier binaire et reconstitue la matrice quantifiée
        decompressionNG_AP_K(factQualite, &matriceDequant);
        // Calcul l'erreur induite par la quantification
        calculErreur(matriceDequant, recupImageNGOpt, (*arbreHaar)->xmax, (*arbreHaar)->ymax);
        // Copie le contenu de la matrice matriceDequant dans DonneesImageRGB
        creeImage(image, matriceDequant, matriceDequant, matriceDequant, (*arbreHaar)->ymax, (*arbreHaar)->xmax);
        // Libère proprement les matrices recupImageNGOpt et matriceDequant créées précédemment
        libereMatrice((*arbreHaar)->ymax, &recupImageNGOpt);
        libereMatrice((*arbreHaar)->ymax, &matriceDequant);
        // Libère proprement l'arbre complet
        libereArbre(arbreHaar);
    } else {
        (*index) = 10;
        // Crée l'arbre binaire correspondant à la matrice seuillage
        creeArbreBinaire(imageModif, arbre);
        // Fusionne les régions voisines et semblables de l'arbre
        fusionNB(arbre);
        noeudBinaire *arbreTemp = (*arbre);
        // Crée le fichier binaire optimisé associé à l'arbre arbreTemp
        compressionBinaireOptSP(arbreTemp);
        short int **recupImageBinaireOpt;
        // Crée la matrice correspondant au contenu du fichier binaire optimisé
        decompressionBinaireOptSP(&recupImageBinaireOpt);
        // Copie le contenu de la matrice recupImageBinaireOpt dans DonneesImageRGB
        creeImage(image, recupImageBinaireOpt, recupImageBinaireOpt, recupImageBinaireOpt, (*arbre)->ymax, (*arbre)->xmax);
        // Libère proprement la matrice recupImageBinaire créée précédemment
        libereMatrice((*arbre)->ymax, &recupImageBinaireOpt);
    }
}

// Change le contenu de DonneesImageRGB par celui de matImage selon le traitement sélectionné sur l'interface
// L'image présente sur l'interface graphique est donc automatiquement affichée avec le nouveau contenu
void clicMenu(DonneesImageRGB **image, matImage *imageModif, int *index, noeudBinaire **arbre, arbreImage **arbreHaar) {
    if(abscisseSouris() >= largeurFenetre() / 16 && abscisseSouris() <= 3 * largeurFenetre() / 16 && ordonneeSouris() >= 0 && ordonneeSouris() <= hauteurFenetre() / 16) {
        (*index) = 1;
        // Copie le contenu des matrices rouge, vert et bleu dans DonneesImageRGB
        creeImage(image, imageModif->rouge, imageModif->vert, imageModif->bleu, imageModif->hauteurImage, imageModif->largeurImage);
    } else if(abscisseSouris() >= 4 * largeurFenetre() / 16 && abscisseSouris() <= 6 * largeurFenetre() / 16 && ordonneeSouris() >= 0 && ordonneeSouris() <= hauteurFenetre() / 16) {
        (*index) = 2;
        // Copie le contenu des matrices rougeInverse, vertInverse et bleuInverse dans DonneesImageRGB
        creeImage(image, imageModif->rougeInverse, imageModif->vertInverse, imageModif->bleuInverse, imageModif->hauteurImage, imageModif->largeurImage);
    } else if(abscisseSouris() >= 7 * largeurFenetre() / 16 && abscisseSouris() <= 9 * largeurFenetre() / 16 && ordonneeSouris() >= 0 && ordonneeSouris() <= hauteurFenetre() / 16) {
        (*index) = 3;
        // Copie le contenu de la matrice niveauGris dans DonneesImageRGB
        creeImage(image, imageModif->niveauGris, imageModif->niveauGris, imageModif->niveauGris, imageModif->hauteurImage, imageModif->largeurImage);
    } else if(abscisseSouris() >= 10 * largeurFenetre() / 16 && abscisseSouris() <= 12 * largeurFenetre() / 16 && ordonneeSouris() >= 0 && ordonneeSouris() <= hauteurFenetre() / 16) {
        (*index) = 4;
        // Copie le contenu de la matrice seuillage dans DonneesImageRGB
        creeImage(image, imageModif->seuillage, imageModif->seuillage, imageModif->seuillage, imageModif->hauteurImage, imageModif->largeurImage);
    } else if(abscisseSouris() >= 13 * largeurFenetre() / 16 && abscisseSouris() <= 15 * largeurFenetre() / 16 && ordonneeSouris() >= 0 && ordonneeSouris() <= hauteurFenetre() / 16) {
        (*index) = 5;
        // Copie le contenu de la matrice filtreMedian dans DonneesImageRGB
        creeImage(image, imageModif->filtreMedian, imageModif->filtreMedian, imageModif->filtreMedian, imageModif->hauteurImage, imageModif->largeurImage);
    } else if (abscisseSouris() >= largeurFenetre() / 16 && abscisseSouris() <= 3 * largeurFenetre() / 16 && ordonneeSouris() >= 15 * hauteurFenetre() / 16 && ordonneeSouris() <= hauteurFenetre()) {
        (*index) = 6;
        // Crée l'arbre binaire correspondant à la matrice seuillage
        creeArbreBinaire(imageModif, arbre);
        // Copie le contenu de la matrice filtreMedian dans DonneesImageRGB
        creeImage(image, imageModif->resultatBinaire, imageModif->resultatBinaire, imageModif->resultatBinaire, imageModif->hauteurImage, imageModif->largeurImage);
    } else if (abscisseSouris() >= 4 * largeurFenetre() / 16 && abscisseSouris() <= 6 * largeurFenetre() / 16 && ordonneeSouris() >= 15 * hauteurFenetre() / 16 && ordonneeSouris() <= hauteurFenetre()) {
        (*index) = 7;
        // Construit l'arbre compressé grâce à l'ondelette de Haar
        creeArbreHaar(image, imageModif, arbreHaar);
    } else if (abscisseSouris() >= 10 * largeurFenetre() / 16 && abscisseSouris() <= 12 * largeurFenetre() / 16 && ordonneeSouris() >= 15 * hauteurFenetre() / 16 && ordonneeSouris() <= hauteurFenetre()) {
        // Effectue les opérations de compression et décompression en binaire et en niveaux de gris
        // Il faut recliquer sur le bouton Sans Perte pour lancer la compression et décompression en niveaux de gris
        compressionDecompressionBinaireSP(image, imageModif, index, arbre, arbreHaar);
    } else if (abscisseSouris() >= 13 * largeurFenetre() / 16 && abscisseSouris() <= 15 * largeurFenetre() / 16 && ordonneeSouris() >= 15 * hauteurFenetre() / 16 && ordonneeSouris() <= hauteurFenetre()) {
        // Effectue les opérations de compression et décompression optimisées en binaire et en niveaux de gris
        // Il faut recliquer sur le bouton Avec Perte pour lancer la compression et décompression optimisées en niveaux de gris
        optimisationCompressionDecompression(image, imageModif, index, arbre, arbreHaar);
    }
}

// Augmente ou diminue le niveau du seuil selon si l'utilisateur sélectionne le bouton '+' ou '-'
void changementSeuil(DonneesImageRGB **image, matImage *imageModif, int *seuil, int index) {
    if(abscisseSouris() >= 29 * largeurFenetre() / 32 && abscisseSouris() <= largeurFenetre() && ordonneeSouris() >= 15 * hauteurFenetre() / 32 && ordonneeSouris() <= 17 * hauteurFenetre() / 32 && index == 4 && (*seuil) < 255) {
        (*seuil)++;
        // Modifie le contenu de la matrice seuillage selon le nouveau seuil
        seuillageNG(imageModif->niveauGris, &(imageModif->seuillage), imageModif->seuil, imageModif->hauteurImage, imageModif->largeurImage);
        // Copie le contenu de la matrice seuillage dans DonneesImageRGB
        creeImage(image, imageModif->seuillage, imageModif->seuillage, imageModif->seuillage, imageModif->hauteurImage, imageModif->largeurImage);
    } else if(abscisseSouris() >= 0 && abscisseSouris() <= 3 * largeurFenetre() / 32 && ordonneeSouris() >= 15 * hauteurFenetre() / 32 && ordonneeSouris() <= 17 * hauteurFenetre() / 32 && index == 4 && (*seuil) > 0) {
        (*seuil)--;
        // Modifie le contenu de la matrice seuillage selon le nouveau seuil
        seuillageNG(imageModif->niveauGris, &(imageModif->seuillage), imageModif->seuil, imageModif->hauteurImage, imageModif->largeurImage);
        // Copie le contenu de la matrice seuillage dans DonneesImageRGB
        creeImage(image, imageModif->seuillage, imageModif->seuillage, imageModif->seuillage, imageModif->hauteurImage, imageModif->largeurImage);
    }
}

// Sauvegarde l'état actuel de l'image avec un nom spécifique au traitement sélectionné
void sauvegarde(DonneesImageRGB *image, nomImage imageInfo, int index) {
    if(abscisseSouris() >= 7 * largeurFenetre() / 16 && abscisseSouris() <= 9 * largeurFenetre() / 16 && ordonneeSouris() >= 15 * hauteurFenetre() / 16 && ordonneeSouris() <= hauteurFenetre())
        if(ecrisBMPRGB_Dans(image, imageInfo.nomImage[index]))
            ecrisBMPRGB_Dans(image, imageInfo.nomImage[index]);
}
