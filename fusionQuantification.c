#include <stdio.h>      // Pour pouvoir utiliser printf()
#include <stdlib.h>     // Pour pouvoir utiliser exit()
#include <string.h>     // Pour pouvoir utiliser malloc()
#include <math.h>       // Pour utiliser les fonctions mathématiques
#include "BmpLib.h"     // Cet include permet de manipuler des fichiers BMP
#include "structure.h"
#include "gereMatrice.h"
#include "compressionBinaire.h"
#include "fusionQuantification.h"

// Détermine le nombre de feuilles dans l'arbre
void nbEtiquette(noeudBinaire *arbre, int *compteur) {
	if(arbre != NULL) {
        // Incrémente le nombre de feuilles lorsque nous sommes sur une feuille
        // (étiquette différente de -1)
		if(arbre->etiq != -1)
            (*compteur)++;
        // Réitère la vérification pour chaque fils
		nbEtiquette(arbre->filsNO, compteur);
		nbEtiquette(arbre->filsNE, compteur);
		nbEtiquette(arbre->filsSO, compteur);
		nbEtiquette(arbre->filsSE, compteur);
	}
}

// Rempli la structure tabArbreBinaire avec les informations de chaque feuille
void rempliTab(noeudBinaire *arbre, tabArbreBinaire **tab, int *compteur) {
    if(arbre != NULL) {
        // Complète une case du tableau puisque nous sommes sur une feuille
		if(arbre->etiq != -1) {
            (*tab)[(*compteur)].coord.xmin = arbre->xmin;
            (*tab)[(*compteur)].coord.xmax = arbre->xmax;
            (*tab)[(*compteur)].coord.ymin = arbre->ymin;
            (*tab)[(*compteur)].coord.ymax = arbre->ymax;
            (*tab)[(*compteur)].valeur = arbre->valeur;
            (*tab)[(*compteur)].etiq = arbre->etiq;
            (*compteur)++;
        }
        // Réitère l'opération pour chaque fils
		rempliTab(arbre->filsNO, tab, compteur);
		rempliTab(arbre->filsNE, tab, compteur);
		rempliTab(arbre->filsSO, tab, compteur);
		rempliTab(arbre->filsSE, tab, compteur);
	}
}

// Met à jour une case du tableau avec la valeur de la nouvelle étiquette
void upTab(tabArbreBinaire **tab, int oldEtiq, int newEtiq, int nbDonnees) {
    for(int i = 0; i < nbDonnees; i++) {
        // On cherche l'élément du tableau qui possède l'ancienne étiquette
        // pour lui affecter la nouvelle
        if((*tab)[i].etiq == oldEtiq)
            (*tab)[i].etiq = newEtiq;
    }
}

// Fusionne les étiquettes des régions voisines et semblables
void optimiseArbreBinaire(noeudBinaire **arbre, tabArbreBinaire **tab, int nbDonnees) {
    if((*arbre) != NULL) {
		if((*arbre)->etiq != -1) {
            for(int temp = 0; temp < nbDonnees; temp++) {
                // Pour chaque données du tableau possédant la même valeur que la feuille
                if((*arbre)->valeur == (*tab)[temp].valeur) {
                    // Vérifie que le carré de l'arbre se situe à droite et est collé au carré
                    // du tableau
                    if((*arbre)->xmin == (*tab)[temp].coord.xmax && (((*arbre)->ymin >= (*tab)[temp].coord.ymin && (*arbre)->ymin < (*tab)[temp].coord.ymax) || ((*arbre)->ymax > (*tab)[temp].coord.ymin && (*arbre)->ymax <= (*tab)[temp].coord.ymax))) {
                        // Met à jour le tableau de référence
                        upTab(tab, (*arbre)->etiq, (*tab)[temp].etiq, nbDonnees);
                        // Met à jour la feuille de l'arbre avec la nouvelle étiquette
                        (*arbre)->etiq = (*tab)[temp].etiq;
                        temp = nbDonnees;
                    // Lorsque le carré de l'arbre se situe en-dessous et est collé au carré
                    // du tableau
                    } else if((*arbre)->ymax == (*tab)[temp].coord.ymin && (((*arbre)->xmin >= (*tab)[temp].coord.xmin && (*arbre)->xmin < (*tab)[temp].coord.xmax) || ((*arbre)->xmax > (*tab)[temp].coord.xmin && (*arbre)->xmax <= (*tab)[temp].coord.xmax))) {
                        // Met à jour le tableau de référence
                        upTab(tab, (*arbre)->etiq, (*tab)[temp].etiq, nbDonnees);
                        // Met à jour la feuille de l'arbre avec la nouvelle étiquette
                        (*arbre)->etiq = (*tab)[temp].etiq;
                        temp = nbDonnees;
                    }
                }
            }
        }
        // Réitère la vérification pour chaque fils
		optimiseArbreBinaire(&((*arbre)->filsNO), tab, nbDonnees);
		optimiseArbreBinaire(&((*arbre)->filsNE), tab, nbDonnees);
		optimiseArbreBinaire(&((*arbre)->filsSO), tab, nbDonnees);
		optimiseArbreBinaire(&((*arbre)->filsSE), tab, nbDonnees);
	}
}

// Corrige les étiquettes d'un arbre blanc et noir, en fusionnant les régions voisines et semblables
void fusionNB(noeudBinaire **arbre) {
    int compteur = 0;
    // Détermine le nombre de feuilles de l'arbre
    nbEtiquette((*arbre), &compteur);

    // Initialise dynamiquement le tableau de référence
    tabArbreBinaire *tabDonnees = (tabArbreBinaire *) malloc(sizeof(tabArbreBinaire) * compteur);
    compteur = 0;
    // Rempli le tableau de référence avec les feuilles de l'arbre
    rempliTab((*arbre), &tabDonnees, &compteur);
    // Fusionne les régions voisines et semblables de l'arbre
    optimiseArbreBinaire(arbre, &tabDonnees, compteur);
    // Libère le tableau de référence
    free(tabDonnees);
    tabDonnees = NULL;
}

// Décale les données du tableau jusqu'à l'indice de départ
void decaleTab(tabArbreBinaire **tabDonneesOpt, int depart, int compteur) {
    for(int j = compteur; j > depart; j--) {
        // Copie les données dans la case qui suit
        (*tabDonneesOpt)[j].coord.xmin = (*tabDonneesOpt)[j - 1].coord.xmin;
        (*tabDonneesOpt)[j].coord.xmax = (*tabDonneesOpt)[j - 1].coord.xmax;
        (*tabDonneesOpt)[j].coord.ymin = (*tabDonneesOpt)[j - 1].coord.ymin;
        (*tabDonneesOpt)[j].coord.ymax = (*tabDonneesOpt)[j - 1].coord.ymax;
        (*tabDonneesOpt)[j].valeur = (*tabDonneesOpt)[j - 1].valeur;
        (*tabDonneesOpt)[j].etiq = (*tabDonneesOpt)[j - 1].etiq;
    }
}

// Permet d'optimiser le tableau tabDonnees en regroupant les cordoonnées des régions
// possédant la même étiquette
void tabOpt(tabArbreBinaire *tabDonnees, tabArbreBinaire **tabDonneesOpt, int *nbDonnees) {
    int compteur = 0;
    for(int i = 0; i < (*nbDonnees); i++) {
        if(i == 0) {
            // Initialise la première case du tableau puisque qu'il n'y a encore aucune données
            // dans le tableau
            (*tabDonneesOpt)[compteur].coord.xmin = tabDonnees[i].coord.xmin;
            (*tabDonneesOpt)[compteur].coord.xmax = tabDonnees[i].coord.xmax;
            (*tabDonneesOpt)[compteur].coord.ymin = tabDonnees[i].coord.ymin;
            (*tabDonneesOpt)[compteur].coord.ymax = tabDonnees[i].coord.ymax;
            (*tabDonneesOpt)[compteur].valeur = tabDonnees[i].valeur;
            (*tabDonneesOpt)[compteur].etiq = tabDonnees[i].etiq;
            compteur++;
        } else {
            // On compare les données du tableau tabDonnees avec celles du tableau optimisé
            // pour déterminer s'il est possible de les regrouper
            for(int j = 0; j < compteur; j++) {
                // Vérifie que le carré se situe à droite et est collé au carré de référence
                // Sans oublié qu'ils possèdent la même étiquette
                if(tabDonnees[i].coord.xmin == (*tabDonneesOpt)[j].coord.xmax && tabDonnees[i].etiq == (*tabDonneesOpt)[j].etiq) {
                    // Vérifie que le carré de droite possède les mêmes ymax et ymin
                    if(tabDonnees[i].coord.ymax == (*tabDonneesOpt)[j].coord.ymax && tabDonnees[i].coord.ymin == (*tabDonneesOpt)[j].coord.ymin) {
                        (*tabDonneesOpt)[j].coord.xmax = tabDonnees[i].coord.xmax;
                        j = compteur;
                    // Vérifie que le carré de droite possède le même ymax mais à un ymin supérieur
                    } else if(tabDonnees[i].coord.ymax == (*tabDonneesOpt)[j].coord.ymax && tabDonnees[i].coord.ymin > (*tabDonneesOpt)[j].coord.ymin) {
                        decaleTab(tabDonneesOpt, j, compteur);

                        (*tabDonneesOpt)[j + 1].coord.xmin = (*tabDonneesOpt)[j].coord.xmin;
                        (*tabDonneesOpt)[j + 1].coord.xmax = (*tabDonneesOpt)[j].coord.xmax;
                        (*tabDonneesOpt)[j + 1].coord.ymin = (*tabDonneesOpt)[j].coord.ymin;
                        (*tabDonneesOpt)[j + 1].coord.ymax = tabDonnees[i].coord.ymin;
                        (*tabDonneesOpt)[j + 1].valeur = (*tabDonneesOpt)[j].valeur;
                        (*tabDonneesOpt)[j + 1].etiq = (*tabDonneesOpt)[j].etiq;
                        
                        (*tabDonneesOpt)[j].coord.xmax = tabDonnees[i].coord.xmax;
                        (*tabDonneesOpt)[j].coord.ymin = tabDonnees[i].coord.ymin;
                        compteur++;
                        j = compteur;
                    // Vérifie que le carré de droite possède le même ymin mais à un ymax inférieur
                    } else if(tabDonnees[i].coord.ymax < (*tabDonneesOpt)[j].coord.ymax && tabDonnees[i].coord.ymin == (*tabDonneesOpt)[j].coord.ymin) {
                        decaleTab(tabDonneesOpt, j, compteur);

                        (*tabDonneesOpt)[j + 1].coord.xmin = (*tabDonneesOpt)[j].coord.xmin;
                        (*tabDonneesOpt)[j + 1].coord.xmax = tabDonnees[i].coord.xmax;
                        (*tabDonneesOpt)[j + 1].coord.ymin = (*tabDonneesOpt)[j].coord.ymin;
                        (*tabDonneesOpt)[j + 1].coord.ymax = tabDonnees[i].coord.ymax;
                        (*tabDonneesOpt)[j + 1].valeur = (*tabDonneesOpt)[j].valeur;
                        (*tabDonneesOpt)[j + 1].etiq = (*tabDonneesOpt)[j].etiq;

                        (*tabDonneesOpt)[j].coord.ymin = tabDonnees[i].coord.ymax;
                        compteur++;
                        j = compteur;
                    // Vérifie que le carré de droite possède un ymax inférieur et un ymin supérieur
                    } else if(tabDonnees[i].coord.ymax < (*tabDonneesOpt)[j].coord.ymax && tabDonnees[i].coord.ymin > (*tabDonneesOpt)[j].coord.ymin) {
                        decaleTab(tabDonneesOpt, j, compteur + 1);
                        
                        (*tabDonneesOpt)[j + 2].coord.xmin = (*tabDonneesOpt)[j].coord.xmin;
                        (*tabDonneesOpt)[j + 2].coord.xmax = (*tabDonneesOpt)[j].coord.xmax;
                        (*tabDonneesOpt)[j + 2].coord.ymin = (*tabDonneesOpt)[j].coord.ymin;
                        (*tabDonneesOpt)[j + 2].coord.ymax = tabDonnees[i].coord.ymin;
                        (*tabDonneesOpt)[j + 2].valeur = (*tabDonneesOpt)[j].valeur;
                        (*tabDonneesOpt)[j + 2].etiq = (*tabDonneesOpt)[j].etiq;

                        (*tabDonneesOpt)[j + 1].coord.xmin = (*tabDonneesOpt)[j].coord.xmin;
                        (*tabDonneesOpt)[j + 1].coord.xmax = tabDonnees[i].coord.xmax;
                        (*tabDonneesOpt)[j + 1].coord.ymin = tabDonnees[i].coord.ymin;
                        (*tabDonneesOpt)[j + 1].coord.ymax = tabDonnees[i].coord.ymax;
                        (*tabDonneesOpt)[j + 1].valeur = (*tabDonneesOpt)[j].valeur;
                        (*tabDonneesOpt)[j + 1].etiq = (*tabDonneesOpt)[j].etiq;

                        (*tabDonneesOpt)[j].coord.ymin = tabDonnees[i].coord.ymax;
                        compteur += 2;
                        j = compteur;
                    // Vérifie les cas suivants :
                    //      Même ymin mais ymax supérieur
                    //      Même ymax mais ymin inférieur
                    //      ymax supérieur et ymin inférieur
                    } else if((tabDonnees[i].coord.ymax > (*tabDonneesOpt)[j].coord.ymax && tabDonnees[i].coord.ymin == (*tabDonneesOpt)[j].coord.ymin) || (tabDonnees[i].coord.ymax == (*tabDonneesOpt)[j].coord.ymax && tabDonnees[i].coord.ymin < (*tabDonneesOpt)[j].coord.ymin) || (tabDonnees[i].coord.ymax > (*tabDonneesOpt)[j].coord.ymax && tabDonnees[i].coord.ymin < (*tabDonneesOpt)[j].coord.ymin)) {
                        (*tabDonneesOpt)[compteur].coord.xmin = tabDonnees[i].coord.xmin;
                        (*tabDonneesOpt)[compteur].coord.xmax = tabDonnees[i].coord.xmax;
                        (*tabDonneesOpt)[compteur].coord.ymin = tabDonnees[i].coord.ymin;
                        (*tabDonneesOpt)[compteur].coord.ymax = tabDonnees[i].coord.ymax;
                        (*tabDonneesOpt)[compteur].valeur = tabDonnees[i].valeur;
                        (*tabDonneesOpt)[compteur].etiq = tabDonnees[i].etiq;
                        
                        compteur++;
                        j = compteur;
                    // Vérifie les cas suivants :
                    //      ymin en dehors des coordonnées de référence mais ymax compris entre
                    //      ymax en dehors des coordonnées de référence mais ymin compris entre
                    } else if((tabDonnees[i].coord.ymax < (*tabDonneesOpt)[j].coord.ymax && tabDonnees[i].coord.ymax > (*tabDonneesOpt)[j].coord.ymin && tabDonnees[i].coord.ymin < (*tabDonneesOpt)[j].coord.ymin) || (tabDonnees[i].coord.ymin < (*tabDonneesOpt)[j].coord.ymax && tabDonnees[i].coord.ymin > (*tabDonneesOpt)[j].coord.ymin && tabDonnees[i].coord.ymax > (*tabDonneesOpt)[j].coord.ymax)) {
                        (*tabDonneesOpt)[compteur].coord.xmin = tabDonnees[i].coord.xmin;
                        (*tabDonneesOpt)[compteur].coord.xmax = tabDonnees[i].coord.xmax;
                        (*tabDonneesOpt)[compteur].coord.ymin = tabDonnees[i].coord.ymin;
                        (*tabDonneesOpt)[compteur].coord.ymax = tabDonnees[i].coord.ymax;
                        (*tabDonneesOpt)[compteur].valeur = tabDonnees[i].valeur;
                        (*tabDonneesOpt)[compteur].etiq = tabDonnees[i].etiq;
                        
                        compteur++;
                        j = compteur;
                    }
                // Dans le cas d'un carré d'une autre couleur donc d'une étiquette différente
                } else if(tabDonnees[i].etiq != -1 && j == compteur - 1) {
                    (*tabDonneesOpt)[j + 1].coord.xmin = tabDonnees[i].coord.xmin;
                    (*tabDonneesOpt)[j + 1].coord.xmax = tabDonnees[i].coord.xmax;
                    (*tabDonneesOpt)[j + 1].coord.ymin = tabDonnees[i].coord.ymin;
                    (*tabDonneesOpt)[j + 1].coord.ymax = tabDonnees[i].coord.ymax;
                    (*tabDonneesOpt)[j + 1].valeur = tabDonnees[i].valeur;
                    (*tabDonneesOpt)[j + 1].etiq = tabDonnees[i].etiq;
                    
                    compteur++;
                    j = compteur;
                }
            }
        }
    }
    (*nbDonnees) = compteur;
}

// Sauvegarde les régions optimisées dans le fichier binaire
void sauveFeuilleBinaireOpt(noeudBinaire *noeud, FILE *f) {
    int compteur = 0;
    // Récupère le nombre de feuilles de l'arbre
    nbEtiquette(noeud, &compteur);
    
    // Initilise dynamiquement le tableau contenant les données de l'arbre
    tabArbreBinaire *tabDonnees = (tabArbreBinaire *) malloc(sizeof(tabArbreBinaire) * compteur);
    compteur = 0;
    // Rempli le tableau avec les données de l'arbre
    rempliTab(noeud, &tabDonnees, &compteur);
    
    // Initialise dynamiquement le tableau contenant les données de l'arbre optimisé
    tabArbreBinaire *tabDonneesOpt = (tabArbreBinaire *) malloc(sizeof(tabArbreBinaire) * compteur);
    // Rempli le tableau avec les données de l'arbre optimisé
    tabOpt(tabDonnees, &tabDonneesOpt, &compteur);

    short int compteurOpt = compteur, xMin = 0, yMin = 0, xMax = 0, yMax = 0;
    // Ecrit le nombre d'optimisations dans le fichier binaire
    fwrite(&compteurOpt, sizeof(short int), 1, f);
    for(int i = 0; i < compteur; i++) {
        xMin = tabDonneesOpt[i].coord.xmin;
        yMin = tabDonneesOpt[i].coord.ymin;
        xMax = tabDonneesOpt[i].coord.xmax;
        yMax = tabDonneesOpt[i].coord.ymax;
        if(tabDonneesOpt[i].valeur == 0)
            xMax = -tabDonneesOpt[i].coord.xmax;
        // Ecrit dans le fichier binaire les coordonnées des régions optimisées
        // La valeur de cette région et déterminée selon si xMax est positif ou négatif
        fwrite(&xMin, sizeof(short int), 1, f);
        fwrite(&xMax, sizeof(short int), 1, f);
        fwrite(&yMin, sizeof(short int), 1, f);
        fwrite(&yMax, sizeof(short int), 1, f);
    }

    // Libère les tableaux contenant les données de l'arbre
    free(tabDonnees);
    free(tabDonneesOpt);
    tabDonnees = NULL;
    tabDonneesOpt = NULL;
}

// Crée un fichier binaire avec un nombre minimal de régions
void compressionBinaireOptSP(noeudBinaire *noeud) {
    // Ouvre le fichier binaire imageBinaireOpt.bin en écriture
    FILE *f = fopen("imageBinaireOpt.bin", "wb");
    // Permet de prévenir des problèmes de lecture
    if (f == NULL)
        return;
    // Initialise l'en-tête du fichier
    short int nbLig = noeud->ymax, nbCol = noeud->xmax;
    char nbBits = 1, typeCompression = 0;

    // Ecrit l'en-tête dans le fichier binaire optimisé
    fwrite(&nbLig, sizeof(short int), 1, f);
    fwrite(&nbCol, sizeof(short int), 1, f);
    fwrite(&nbBits, sizeof(char), 1, f);
    fwrite(&typeCompression, sizeof(char), 1, f);

    // Sauvegarde les données de chaque région dans le fichier binaire optimisé
    sauveFeuilleBinaireOpt(noeud, f);
    // Ferme le fichier
    fclose(f);
}

// Lit le fichier binaire et complète la matrice
void sauveMatriceBinaireOpt(short int ***recupImage, FILE *f) {
	// Initialise des variables locales qui vont contenir les coordonnées
	// à stocker dans la matrice
	short int compteur = 0, xMin = 0, yMin = 0, xMax = 0, yMax = 0;
    
    // Récupère le nombre d'optimisations dans le fichier binaire
    fread(&compteur, sizeof(short int), 1, f);
    for(int i = 0; i < compteur; i++) {
        // Lit chaque coordonnées des régions
        fread(&xMin, sizeof(short int), 1, f);
        fread(&xMax, sizeof(short int), 1, f);
        fread(&yMin, sizeof(short int), 1, f);
        fread(&yMax, sizeof(short int), 1, f);

        // Rempli la matrice finale avec la couleur récupérée précédemment
        // Elle est caché dans xMax selon s'il est positif ou négatif
        for(int k = yMin; k < yMax; k++) {
            for(int l = xMin; l < abs(xMax); l++) {
                if(xMax < 0)
                    (*recupImage)[k][l] = 0;
                else
                    (*recupImage)[k][l] = 255;
            }
        }
    }
}

// Crée la matrice correspondant à un fichier binaire optimisé
void decompressionBinaireOptSP(short int ***recupImage) {
	// Ouvre le fichier binaire imageBinaireOpt.bin en lecture
	FILE *f = fopen("imageBinaireOpt.bin", "rb");
	// Permet de prévenir des problèmes de lecture
	if(f == NULL)
		return;
	// Initialise des variables locales qui vont contenir l'en-tête du fichier
	short int nbLig = 0, nbCol = 0;
    char nbBits = 0, typeCompression = 0;

	// Lit l'en-tête du fichier binaire
    fread(&nbLig, sizeof(short int), 1, f);
    fread(&nbCol, sizeof(short int), 1, f);
    fread(&nbBits, sizeof(char), 1, f);
    fread(&typeCompression, sizeof(char), 1, f);

	// Initialise dynamiquement la matrice recupImage
	(*recupImage) = (short int **) malloc(sizeof(short int *) * nbLig);
	for(int i = 0; i < nbLig; i++)
        (*recupImage)[i] = (short int *) malloc(sizeof(short int) * nbCol);

	// Récupère les données du fichier dans la matrice recupImage
	sauveMatriceBinaireOpt(recupImage, f);
	// Ferme le fichier
	fclose(f);
}

// Crée la matrice de quantification à partir de ses dimensions et du facteur de qualité k
void creeMatriceQK(int ***matriceQK, int k, int largeur, int hauteur) {
    // La formule utilisé est k * 10.2 puisqu'avec celle fournit dans l'énoncé,
    // l'image résultante comporte des incohérences pour un facteur de qualité faible
    for(int i = hauteur - 1; i > -1; i--)
        for(int j = 0; j < largeur; j++)
            (*matriceQK)[i][j] = k * 10.2; //1 + k * (1 + hauteur + 1 - i + j);
}

// Calcul la matrice quantifiée à partir d'une matrice image et d'une matrice de quantification
void quantifieMatrice(int ***matriceQuant, int **matriceQK, short int **image, int largeur, int hauteur) {
    // On divise ici la matrice image par la matrice de quantification déterminée dans creeMatriceQK()
    for(int i = hauteur - 1; i > -1; i--)
        for(int j = 0; j < largeur; j++)
            (*matriceQuant)[i][j] = image[i][j] / matriceQK[i][j];
}

// Réalise l'opération duale de quantifieMatrice()
void dequantifieMatrice(short int ***matriceDequant, int **matriceQK, short int **matriceQuant, int largeur, int hauteur) {
    // On multiplie cette fois la matrice quantifée par la matrice de quantification pour obtenir l'image
    for(int i = 0; i < hauteur; i++)
        for(int j = 0; j < largeur; j++)
            (*matriceDequant)[i][j] = matriceQuant[i][j] * matriceQK[i][j];
}

// Calcul l'erreur induite par la quantification
void calculErreur(short int **matriceDequant, short int **image, int largeur, int hauteur) {
    int erreur = 0;
    // L'erreur est ici calculée avec la somme des matrices soustraites termes à termes au carré
    for(int i = 1; i < hauteur; i++)
        for(int j = 1; j < largeur; j++)
            erreur += (image[i][j]- matriceDequant[i][j]) * (image[i][j] - matriceDequant[i][j]);
    printf("Erreur de la quantification : %f\n", sqrt(erreur));
}

// Permet de transformer une matrice en un tableau à l'aide d'un parcourt en zigzag
void parcourtZigzagInt(int **matriceQuant, short int **tabZigzag, int i, int j, int indice, int taille) {
	int compteur = 0;
	// On parcourt en zigzag chaque pixel de la matrice
	while(j > -1 && j < taille && i > -1 && i < taille) {
		// Stock la valeur de la matrice dans le tableau
		(*tabZigzag)[compteur] = matriceQuant[j][i];
		compteur++;
		// Une fois en haut ou en bas de la matrice, on se décale de 1 pixel pour continuer le parcourt
		if((j == taille - 1 && indice == 0) || (j == 0 && indice == 1)) {
			i += 1;
			indice = (indice + 1) % 2;
		// Une fois à gauche ou à droite de la matrice, on se décale de 1 pixel pour continuer le parcourt
		} else if((i == taille - 1 && indice == 0) || (i == 0 && indice == 1)) {
			j -= 1;
			indice = (indice + 1) % 2;
		// Dans le cas restant, on monte ou on descend en diagonale
		} else
			(indice == 0) ? (i++, j++) : (i--, j--);
	}
}

// Permet de sauvegarder les données d'une matrice quantifiée dans un fichier
void sauveFeuilleNG_AP_K(int **matriceQuant, FILE *f, int factQualite, short int hauteur, short int largeur) {
	// Initialise dynamiquement un tableau contenant les résultats du parcourt en zigzag
    short int *tabZigzag = (short int *) malloc(sizeof(short int) * largeur * hauteur);
    // Permet de transformer la matrice matriceQuant en un tableau à l'aide d'un parcourt en zigzag
    parcourtZigzagInt(matriceQuant, &tabZigzag, 0, hauteur - 1, 0, hauteur);

    // Permet de stocker le nombre d'optimisations
    int nbOptimisation = 0;
    // Détermine le nombre d'optimsations de tabZigzag
    placeOptimise(tabZigzag, hauteur, &nbOptimisation);
    // Ecrit le nombre d'optimisations dans le fichier
    fwrite(&nbOptimisation, sizeof(int), 1, f);
    
    // Initialise dynamiquement deux tableau dont un contenant le parcourt en zigzag optimisé
    // et l'autre le nombre d'itérations de chaque valeur du tableau précédent
    short int *tabZigzagOpti = (short int *) malloc(sizeof(short int) * nbOptimisation);
    unsigned short int *tabZigzagIte = (unsigned short int *) malloc(sizeof(unsigned short int) * nbOptimisation);

    // Initialise le tableau d'itérations pour éviter d'avoir des valeurs différents de 0 au départ
    for(int i = 0; i < nbOptimisation; i++)
        tabZigzagIte[i] = 0;

    // Optimise le tableau en zigzag et détermine le nombre d'itérations de chaque valeur
    optimiseTableau(&tabZigzagOpti, &tabZigzagIte, tabZigzag, hauteur);
    // Ecrit les tableaux optimisés dans le fichier
    fwrite(tabZigzagOpti, sizeof(short int), nbOptimisation, f);
    fwrite(tabZigzagIte, sizeof(unsigned short int), nbOptimisation, f);

    // Libère proprement les tableaux créés précédemment
    libereTableauZigzag(&tabZigzag, &tabZigzagOpti, &tabZigzagIte);
}

// Crée le fichier binaire d'une image compressée contenant le parcourt en zigzag de la matrice quantifiée
void compressionNG_AP_K(arbreImage **arbreHaar, int factQualite, short int **recupImageNGOpt) {
	// Ouvre le fichier binaire imageNG_AP_K.bin en écriture
	FILE *f = fopen("imageNG_AP_K.bin", "wb");
	// Permet de prévenir des problèmes de lecture
	if(f == NULL)
		return;
	// Initialise l'en-tête du fichier
	short int nbLig = (*arbreHaar)->ymax - (*arbreHaar)->ymin, nbCol = (*arbreHaar)->xmax - (*arbreHaar)->xmin;
    char nbBits = 8, typeCompression = factQualite;

	// Ecrit l'en-tête dans le fichier binaire
	fwrite(&nbLig, sizeof(short int), 1, f);
    fwrite(&nbCol, sizeof(short int), 1, f);
    fwrite(&nbBits, sizeof(char), 1, f);
    fwrite(&typeCompression, sizeof(char), 1, f);

    // Initialise dynamiquement les trois matrices matriceQK, matriceQuant et matriceDequant
    // nécessaires aux étapes de quantification
    int **matriceQK = (int **) malloc(sizeof(int *) * (*arbreHaar)->ymax);
    int **matriceQuant = (int **) malloc(sizeof(int *) * (*arbreHaar)->ymax);
    short int **matriceDequant = (short int **) malloc(sizeof(short int *) * (*arbreHaar)->ymax);
    for(int i = 0; i < (*arbreHaar)->ymax; i++) {
        matriceQK[i] = (int *) malloc(sizeof(int) * (*arbreHaar)->xmax);
        matriceQuant[i] = (int *) malloc(sizeof(int) * (*arbreHaar)->xmax);
        matriceDequant[i] = (short int *) malloc(sizeof(short int) * (*arbreHaar)->xmax);
    }
    // Crée la matrice de quantification
    creeMatriceQK(&matriceQK, factQualite, (*arbreHaar)->xmax, (*arbreHaar)->ymax);
    // Crée la matrice quantifiée en multipliant la matrice image est la matrice matriceQK
    quantifieMatrice(&matriceQuant, matriceQK, recupImageNGOpt, (*arbreHaar)->xmax, (*arbreHaar)->ymax);
	
	// Sauvegarde les données de la matrice quantifiée dans le fichier binaire
	sauveFeuilleNG_AP_K(matriceQuant, f, factQualite, nbLig, nbCol);
    // Libère proprement les matrices matriceQK, matriceQuant et matriceDequant créées précédemment
    libereMatriceEntier((*arbreHaar)->ymax, &matriceQK);
    libereMatriceEntier((*arbreHaar)->ymax, &matriceQuant);
    libereMatrice((*arbreHaar)->ymax, &matriceDequant);
	// Ferme le fichier
	fclose(f);
}

// Récupére les données du fichier binaire dans une matrice
void sauveMatriceNG_AP_K(short int ***recupImage, FILE *f, int taille) {
    // Initialise la variable locale qui va contenir le nombre d'optimisations à stocker dans la matrice
	int nbOptimisation = 0;

	// Procède à une lecture du fichier s'il contient des données
	// Ecrit le nombre d'optimisations contenus dans le fichier
	if(fread(&nbOptimisation, sizeof(int), 1, f)) {
		// Initialise dynamiquement deux tableau dont un contenant le parcourt en zigzag optimisé
		// et l'autre le nombre d'itérations de chaque valeur du tableau précédent
		short int *tabZigzagOpti = (short int *)malloc(sizeof(short int) * nbOptimisation);
		unsigned short int *tabZigzagIte = (unsigned short int *)malloc(sizeof(unsigned short int) * nbOptimisation);
		// Complète les deux tableaux précédent à l'aide du fichier
		fread(tabZigzagOpti, sizeof(short int), nbOptimisation, f);
		fread(tabZigzagIte, sizeof(unsigned short int), nbOptimisation, f);

		// Initialise dynamiquement un tableau contenant les valeurs du parcourt en zigzag non-optimisé
		short int *tabZigzag = (short int *) malloc(sizeof(short int) * taille * taille);
		// Copie les données du tableau en zigzag optimisé dans celui non-optimisé
		deOptimiseTableau(&tabZigzag, tabZigzagOpti, tabZigzagIte, nbOptimisation);

		// Recompose une matrice à partir du tableau contenant les valeurs du parcourt en zigzag
		recompositionZigzag(recupImage, tabZigzag, 0, taille - 1, 0, taille);
        
		// Libère proprement les tableaux tabZigzag, tabZigzagOpti et tabZigzagIte créée précédemment
		libereTableauZigzag(&tabZigzag, &tabZigzagOpti, &tabZigzagIte);
	}
}

// Reconstitue l'image compressé dans le fichier binaire imageNG_AP_K
void decompressionNG_AP_K(int factQualite, short int ***matriceDequant) {
	// Ouvre le fichier binaire imageNG_AP_K.bin en lecture
	FILE *f = fopen("imageNG_AP_K.bin", "rb");
	// Permet de prévenir des problèmes de lecture
	if(f == NULL)
		return;
	// Initialise des variables locales qui vont contenir l'en-tête du fichier
	short int nbLig = 0, nbCol = 0;
    char nbBits = 0, typeCompression = 0;

	// Lit l'en-tête du fichier binaire
	fread(&nbLig, sizeof(short int), 1, f);
    fread(&nbCol, sizeof(short int), 1, f);
    fread(&nbBits, sizeof(char), 1, f);
    fread(&typeCompression, sizeof(char), 1, f);

	// Initialise dynamiquement la matrice recupImage
	short int **recupImage = (short int **) malloc(sizeof(short int *) * nbLig);
	for(int i = 0; i < nbLig; i++)
        recupImage[i] = (short int *) malloc(sizeof(short int) * nbCol);

	// Récupère les données du fichier dans la matrice recupImage
	sauveMatriceNG_AP_K(&recupImage, f, nbLig);
    
    // Initialise dynamiquement les trois matrices matriceQK, matriceQuant et matriceDequant
    // nécessaires aux étapes de quantification
    int **matriceQK = (int **) malloc(sizeof(int *) * nbLig);
    (*matriceDequant) = (short int **) malloc(sizeof(short int *) * nbLig);
    for(int i = 0; i < nbLig; i++) {
        matriceQK[i] = (int *) malloc(sizeof(int) * nbCol);
        (*matriceDequant)[i] = (short int *) malloc(sizeof(short int) * nbCol);
    }
    // Crée la matrice de quantification
    creeMatriceQK(&matriceQK, factQualite, nbCol, nbLig);
    // Effectue l'opération duale de quantifieMatrice()
    dequantifieMatrice(matriceDequant, matriceQK, recupImage, nbCol, nbCol);
    // Libère proprement les matrices matriceQK et matriceQuant créées précédemment
    libereMatriceEntier(nbLig, &matriceQK);
    libereMatrice(nbLig, &recupImage);
	// Ferme le fichier
	fclose(f);
}

// Libère proprement la matrice d'entiers
void libereMatriceEntier(int hauteurImage, int ***image2Arbre) {
    // Désallocation de la matrice image2Arbre avec affectation de la valeur NULL
    for(int i = 0; i < hauteurImage; i++) {
        free((*image2Arbre)[i]);
        (*image2Arbre)[i] = NULL;
    }

    free((*image2Arbre));
    (*image2Arbre) = NULL;
}