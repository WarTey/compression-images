#include <stdlib.h>			// Pour pouvoir utiliser exit()
#include <math.h>			// Pour pouvoir utiliser sin() et cos()
#include "GfxLib.h"			// Seul cet include est necessaire pour faire du graphique
#include "BmpLib.h"			// Cet include permet de manipuler des fichiers BMP
#include "ESLib.h"			// Pour utiliser valeurAleatoire()
#include "structure.h"
#include "gereMenu.h"

// Largeur et hauteur par defaut d'une image correspondant à nos critères
#define LargeurFenetre 1399
#define HauteurFenetre 612

// La fonction de gestion des évènements, appelée automatiquement par le système
// dès qu'un évènement survient
void gestionEvenement(EvenementGfx evenement);

int main(int argc, char **argv) {
	initialiseGfx(argc, argv);
	prepareFenetreGraphique("Mini-Projet", LargeurFenetre, HauteurFenetre);

	// Lance la boucle qui aiguille les évènements sur la fonction gestionEvenement ci-après,
	// qui elle-même utilise fonctionAffichage ci-dessous
	lanceBoucleEvenements();
	return 0;
}

// La fonction de gestion des évènements, appelée automatiquement par le système
// dès qu'un évènement survient
void gestionEvenement(EvenementGfx evenement) {
	// L'image à afficher à droite de l'écran
	static DonneesImageRGB *image = NULL;
	// L'image à afficher à gauche de l'écran (image de comparaison)
	static DonneesImageRGB *imageTemoin = NULL;

	// On intialise les différentes variables nécessaires aux traitements de l'image
	static nomImage imageInfo;
	static matImage *imageModif = NULL;
	static bouton boutonInfos;
	static noeudBinaire *arbre = NULL;
	static arbreImage *arbreHaar = NULL;

	// Vérifie la bonne dimension de l'image
	static int erreurDimension;

	switch(evenement) {
		case Initialisation:
			/* Le message "Initialisation" est envoye une seule fois, au debut du
			programme : il permet de fixer "image" à la valeur qu'il devra conserver
			jusqu'à la fin du programme : soit "image" reste a NULL si l'image n'a
			pas pu être lue, soit "image" pointera sur une structure contenant
			les caracteristiques de l'image "imageNB.bmp" */

			// On initialise les structures
			initImage(&imageInfo);
			initBouton(&boutonInfos);
			initMatrices(&imageModif, &image, imageInfo.nomImage[0]);
			initImageTemoin(&imageTemoin, image);
			initArbre(&arbre, imageModif);

			// Redimensionne la fenêtre selon la taille de l'image
			//if(image != NULL)
				//redimensionneFenetre(image->largeurImage + 887, image->hauteurImage + 100);

			// On détermine si l'image existe et qu'elle possède les bonnes dimensions
			if(image != NULL)
				for(int i = 2; i < DIM_IMG_MAX + 1; i *= 2)
					if(i == image->largeurImage)
						erreurDimension = 1;

			// Configure le système pour générer un message Temporisation
			// toutes les 20 millisecondes
			demandeTemporisation(20);
			break;

		case Temporisation:
			rafraichisFenetre();
			break;

		case Affichage:
			// On part d'un fond d'écran blanc
			effaceFenetre(255, 255, 255);
			// On affiche les boutons et les images sur l'interface
			affichage(image, imageTemoin, imageInfo, boutonInfos, erreurDimension);
			break;

		case Clavier:
			switch(caractereClavier()) {
				case 'Q':
				case 'q':
					// Libère propement toutes les variables on fin de programme
					if(image != NULL)
						libereMatrices(&imageModif, &image, &imageTemoin, &arbre);
					termineBoucleEvenements();
					break;
			}
			break;

		case ClavierSpecial:
			break;

		case BoutonSouris:
			if(etatBoutonSouris() == GaucheAppuye && image != NULL && erreurDimension == 1) {
				// Effectue l'opération correspondante au traitement sélectionné
				clicMenu(&image, imageModif, &(boutonInfos.index), &arbre, &arbreHaar);
				// Change le seuil de l'image
				changementSeuil(&image, imageModif, &(imageModif->seuil), boutonInfos.index);
				// Sauvegarde l'état actuel de l'image
				sauvegarde(image, imageInfo, boutonInfos.index);
			}
			break;

		case Souris:
			break;

		case Inactivite:
			break;

		case Redimensionnement:
			break;
	}
}
