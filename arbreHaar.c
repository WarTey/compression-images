#include <stdlib.h> 		// Pour pouvoir utiliser exit()
#include <string.h>			// Pour pouvoir utiliser malloc()
#include "BmpLib.h"			// Cet include permet de manipuler des fichiers BMP
#include "structure.h"
#include "gereMatrice.h"
#include "arbreHaar.h"

// Initialise dynamiquement un noeud de l'arbre arbreImage
void alloueFils(arbreImage **noeud, int xmin, int ymin, int xmax, int ymax) {
	// Initialise dynamiquement le noeud
	(*noeud) = (arbreImage *) malloc(sizeof(arbreImage));

	// Stocke chaque valeur xmin, ymin, xmax et xmax dans le noeud
	(*noeud)->xmin = xmin;
	(*noeud)->ymin = ymin;
	(*noeud)->xmax = xmax;
	(*noeud)->ymax = ymax;

	// Initialise dynamiquement la matrice imageDivisee
	(*noeud)->imageDivisee = (short int **) malloc(sizeof(short int *) * (ymax - ymin));
	for(int i = 0; i < ymax - ymin; i++)
        (*noeud)->imageDivisee[i] = (short int *) malloc(sizeof(short int) * (xmax - xmin));

	// Met les fils à NULL puisqu'il s'agit pour l'instant d'une feuille
    (*noeud)->filsNO = NULL;
	(*noeud)->filsNE = NULL;
	(*noeud)->filsSO = NULL;
	(*noeud)->filsSE = NULL;
}

// Crée le sous-arbre associé, correspondant à un niveau de décomposition
void decomposeImage(arbreImage **noeud, int largeurImage, int hauteurImage) {
	// Calcul la hauteur et la largeur du noeud actuel dans deux variables locales
	int hauteurArbre = ((*noeud)->ymax - (*noeud)->ymin) / 2;
	int largeurArbre = ((*noeud)->xmax - (*noeud)->xmin) / 2;

	// Initialise chaque fils du noeud de l'arbre
	alloueFils(&((*noeud)->filsNO), 0, hauteurImage - hauteurArbre, largeurArbre, hauteurImage);
	alloueFils(&((*noeud)->filsNE), largeurArbre, hauteurImage - hauteurArbre, 2 * largeurArbre, hauteurImage);
	alloueFils(&((*noeud)->filsSO), 0, hauteurImage - 2 * hauteurArbre, largeurArbre, hauteurImage - hauteurArbre);
	alloueFils(&((*noeud)->filsSE), largeurArbre, hauteurImage - 2 * hauteurArbre, 2 * largeurArbre, hauteurImage - hauteurArbre);

	// Détermine la matrice imageDivisee pour chaque fils NO, NE, SO et SE
	// La hauteur et largeur des fils sont divisées par deux par rapport à celles du noeud
	for(int i = 0; i < hauteurArbre; i++) {
        for(int j = 0; j < largeurArbre; j++) {
			(*noeud)->filsNO->imageDivisee[i][j] = ((*noeud)->imageDivisee[i*2][j*2] + (*noeud)->imageDivisee[i*2][(j*2)+1])/2;
			(*noeud)->filsNO->imageDivisee[i][j] = ((*noeud)->filsNO->imageDivisee[i][j] + ((*noeud)->imageDivisee[(i*2)+1][j*2] + (*noeud)->imageDivisee[(i*2)+1][(j*2)+1])/2)/2;
			(*noeud)->filsNE->imageDivisee[i][j] = ((*noeud)->imageDivisee[i*2][j*2] + (*noeud)->imageDivisee[i*2][(j*2)+1])/2;
			(*noeud)->filsNE->imageDivisee[i][j] = ((*noeud)->filsNE->imageDivisee[i][j] - ((*noeud)->imageDivisee[(i*2)+1][j*2] + (*noeud)->imageDivisee[(i*2)+1][(j*2)+1])/2)/2;
			(*noeud)->filsSO->imageDivisee[i][j] = ((*noeud)->imageDivisee[i*2][j*2] - (*noeud)->imageDivisee[i*2][(j*2)+1])/2;
			(*noeud)->filsSO->imageDivisee[i][j] = ((*noeud)->filsSO->imageDivisee[i][j] + ((*noeud)->imageDivisee[(i*2)+1][j*2] - (*noeud)->imageDivisee[(i*2)+1][(j*2)+1])/2)/2;
			(*noeud)->filsSE->imageDivisee[i][j] = ((*noeud)->imageDivisee[i*2][j*2] - (*noeud)->imageDivisee[i*2][(j*2)+1])/2;
			(*noeud)->filsSE->imageDivisee[i][j] = ((*noeud)->filsSE->imageDivisee[i][j] - ((*noeud)->imageDivisee[(i*2)+1][j*2] - (*noeud)->imageDivisee[(i*2)+1][(j*2)+1])/2)/2;
		}
	}

	// Libère le noeud NO de l'arbre après avoir déterminé chaque fils
	libereMatrice(2 * hauteurArbre, &((*noeud)->imageDivisee));
}

// Crée le noeud racine correspondant puis utilise la fonction decomposeImage pour créer l'arbre complet
// en décomposant uniquement les images NO pour passer d'un niveau à l'autre
void decomposeNniveaux(matImage *imageModif, arbreImage **noeud, int niveaux) {
	if((*noeud) == NULL) {
		// Initialise dynamiquement la racine de l'arbre
		alloueFils(noeud, 0, 0, imageModif->largeurImage, imageModif->hauteurImage);

		// Complète le contenu de la racine avec l'image en niveaux de gris
		for(int i = 0; i < imageModif->hauteurImage; i++)
	        for(int j = 0; j < imageModif->largeurImage; j++)
				(*noeud)->imageDivisee[i][j] = imageModif->niveauGris[i][j];
	}

	// On vérifie que l'image à traiter possède bien une dimension (2^N x 2^N)
	if((*noeud)->ymax - (*noeud)->ymin > 1 && niveaux != 0) {
		if(niveaux > 1) {
			// Décompose l'image jusqu'au niveau saisi par l'utilsateur
			niveaux -= 1;
			decomposeImage(noeud, imageModif->largeurImage, imageModif->hauteurImage);
			decomposeNniveaux(imageModif, &((*noeud)->filsNO), niveaux);
		} else if(niveaux < 1) {
			// Décompose l'image autant que possible
			decomposeImage(noeud, imageModif->largeurImage, imageModif->hauteurImage);
			decomposeNniveaux(imageModif, &((*noeud)->filsNO), niveaux);
		}
	}
}

// Sauvegarde l'image correspondant à un niveaux de décomposition
void sauveArbreImage(DonneesImageRGB **image, arbreImage **noeud, int niveaux, int compteur) {
	if(compteur < niveaux && (*noeud)->filsNO != NULL) {
		// Calcul la hauteur et la largeur de l'image dans deux variables locales
		int largeurImage = (*image)->largeurImage, hauteurImage = (*image)->hauteurImage;
		// Calcul la hauteur et la largeur du noeud dans deux variables locales
		int largeurArbre = (*noeud)->filsNE->ymax - (*noeud)->filsNE->ymin;
		int hauteurArbre = (*noeud)->filsNO->ymax - (*noeud)->filsNO->ymin;

		// Une fois tout en bas de l'arbre ou au niveau de décomposition demandé par l'utilsateur,
		// copie le contenu de la matrice imageDivisee (filsNO) dans le tableau donneesRGB
		if(compteur == niveaux - 1) {
			// Descend jusqu'en bas de l'arbre pour reconstituer tous les filsNO
			// et ainsi obtenir le contenu de imageDivisee
			recomposeImage(noeud);
			for(int i = (*noeud)->filsNO->ymin; i < (*noeud)->filsNO->ymax; i++) {
				for(int j = (*noeud)->filsNO->xmin; j < (*noeud)->filsNO->xmax; j++) {
					(*image)->donneesRGB[(i * largeurImage + j) * 3] = (*noeud)->filsNO->imageDivisee[i - hauteurImage + hauteurArbre][j];
					(*image)->donneesRGB[(i * largeurImage + j) * 3 + 1] = (*noeud)->filsNO->imageDivisee[i - hauteurImage + hauteurArbre][j];
					(*image)->donneesRGB[(i * largeurImage + j) * 3 + 2] = (*noeud)->filsNO->imageDivisee[i - hauteurImage + hauteurArbre][j];
				}
			}
			// Libère le filsNO une fois la copie terminée
			libereFilsNO(noeud);
		}

		// Copie le contenu de la matrice imageDivisee (filsNE) dans le tableau donneesRGB
		hauteurArbre = (*noeud)->filsNE->ymax - (*noeud)->filsNE->ymin;
		for(int i = (*noeud)->filsNE->ymin; i < (*noeud)->filsNE->ymax; i++) {
			for(int j = (*noeud)->filsNE->xmin; j < (*noeud)->filsNE->xmax; j++) {
				(*image)->donneesRGB[(i * largeurImage + j) * 3] = (*noeud)->filsNE->imageDivisee[i - hauteurImage + hauteurArbre][j - largeurArbre];
				(*image)->donneesRGB[(i * largeurImage + j) * 3 + 1] = (*noeud)->filsNE->imageDivisee[i - hauteurImage + hauteurArbre][j - largeurArbre];
				(*image)->donneesRGB[(i * largeurImage + j) * 3 + 2] = (*noeud)->filsNE->imageDivisee[i - hauteurImage + hauteurArbre][j - largeurArbre];
			}
		}

		// Copie le contenu de la matrice imageDivisee (filsSO) dans le tableau donneesRGB
		hauteurArbre = (*noeud)->filsSO->ymax - (*noeud)->filsSO->ymin;
		for(int i = (*noeud)->filsSO->ymin; i < (*noeud)->filsSO->ymax; i++) {
			for(int j = (*noeud)->filsSO->xmin; j < (*noeud)->filsSO->xmax; j++) {
				(*image)->donneesRGB[(i * largeurImage + j) * 3] = (*noeud)->filsSO->imageDivisee[i - hauteurImage + 2 * hauteurArbre][j];
				(*image)->donneesRGB[(i * largeurImage + j) * 3 + 1] = (*noeud)->filsSO->imageDivisee[i - hauteurImage + 2 * hauteurArbre][j];
				(*image)->donneesRGB[(i * largeurImage + j) * 3 + 2] = (*noeud)->filsSO->imageDivisee[i - hauteurImage + 2 * hauteurArbre][j];
			}
		}

		// Copie le contenu de la matrice imageDivisee (filsSE) dans le tableau donneesRGB
		largeurArbre = (*noeud)->filsSE->xmax - (*noeud)->filsSE->xmin;
		hauteurArbre = (*noeud)->filsSE->ymax - (*noeud)->filsSE->ymin;
		for(int i = (*noeud)->filsSE->ymin; i < (*noeud)->filsSE->ymax; i++) {
			for(int j = (*noeud)->filsSE->xmin; j < (*noeud)->filsSE->xmax; j++) {
				(*image)->donneesRGB[(i * largeurImage + j) * 3] = (*noeud)->filsSE->imageDivisee[i - hauteurImage + 2 * hauteurArbre][j - largeurArbre];
				(*image)->donneesRGB[(i * largeurImage + j) * 3 + 1] = (*noeud)->filsSE->imageDivisee[i - hauteurImage + 2 * hauteurArbre][j - largeurArbre];
				(*image)->donneesRGB[(i * largeurImage + j) * 3 + 2] = (*noeud)->filsSE->imageDivisee[i - hauteurImage + 2 * hauteurArbre][j - largeurArbre];
			}
		}

		// Réitère le processus pour les 4 fils suivants
		sauveArbreImage(image, &((*noeud)->filsNO), niveaux, compteur + 1);
	} else if(compteur == 0 && (*noeud) != NULL) {
		int largeurImage = (*image)->largeurImage, hauteurImage = (*image)->hauteurImage;
		int hauteurArbre = (*noeud)->ymax - (*noeud)->ymin;

		if((*noeud)->filsNO != NULL)
			recomposeImage(noeud);
		for(int i = (*noeud)->ymin; i < (*noeud)->ymax; i++) {
			for(int j = (*noeud)->xmin; j < (*noeud)->xmax; j++) {
				(*image)->donneesRGB[(i * largeurImage + j) * 3] = (*noeud)->imageDivisee[i - hauteurImage + hauteurArbre][j];
				(*image)->donneesRGB[(i * largeurImage + j) * 3 + 1] = (*noeud)->imageDivisee[i - hauteurImage + hauteurArbre][j];
				(*image)->donneesRGB[(i * largeurImage + j) * 3 + 2] = (*noeud)->imageDivisee[i - hauteurImage + hauteurArbre][j];
			}
		}
	}
}

// Permet de reconstituer une image à partir de l'arbre correspondant
void recomposeImage(arbreImage **noeud) {
	if((*noeud)->imageDivisee == NULL) {
		// Descend jusqu'en bas de l'arbre
		recomposeImage(&((*noeud)->filsNO));

		// Calcul la hauteur et la largeur du noeud dans deux variables locales
		int largeurArbre = (*noeud)->xmax - (*noeud)->xmin, hauteurArbre = (*noeud)->ymax - (*noeud)->ymin;

		// Initialise dynamiquement la matrice imageDivisee
		(*noeud)->imageDivisee = (short int **) malloc(sizeof(short int *) * hauteurArbre);
		for(int i = 0; i < hauteurArbre; i++)
			 (*noeud)->imageDivisee[i] = (short int *) malloc(sizeof(short int) * largeurArbre);

		// Reconstitue le noeud à partir des 4 fils NO, NE, SO et SE
		// Si un des résultats est négatif, on prends alors sa valeur absolue pour éviter les erreurs dans l'image résultante
		for(int i = 0; i < hauteurArbre/2; i++) {
			for(int j = 0; j < largeurArbre/2; j++) {
				(*noeud)->imageDivisee[i*2][j*2] = (*noeud)->filsNO->imageDivisee[i][j] + (*noeud)->filsNE->imageDivisee[i][j] + (*noeud)->filsSO->imageDivisee[i][j] + (*noeud)->filsSE->imageDivisee[i][j];
				(*noeud)->imageDivisee[i*2][j*2+1] = (*noeud)->filsNO->imageDivisee[i][j] + (*noeud)->filsNE->imageDivisee[i][j] - (*noeud)->filsSO->imageDivisee[i][j] - (*noeud)->filsSE->imageDivisee[i][j];
				(*noeud)->imageDivisee[i*2+1][j*2] = (*noeud)->filsNO->imageDivisee[i][j] - (*noeud)->filsNE->imageDivisee[i][j] + (*noeud)->filsSO->imageDivisee[i][j] - (*noeud)->filsSE->imageDivisee[i][j];
				(*noeud)->imageDivisee[i*2+1][j*2+1] = (*noeud)->filsNO->imageDivisee[i][j] - (*noeud)->filsNE->imageDivisee[i][j] - (*noeud)->filsSO->imageDivisee[i][j] + (*noeud)->filsSE->imageDivisee[i][j];
				if((*noeud)->imageDivisee[i*2][j*2] < 0)
					(*noeud)->imageDivisee[i*2][j*2] = abs((*noeud)->imageDivisee[i*2][j*2]);
				if((*noeud)->imageDivisee[i*2][j*2+1] < 0)
					(*noeud)->imageDivisee[i*2][j*2+1] = abs((*noeud)->imageDivisee[i*2][j*2+1]);
				if((*noeud)->imageDivisee[i*2+1][j*2] < 0)
					(*noeud)->imageDivisee[i*2+1][j*2] = abs((*noeud)->imageDivisee[i*2+1][j*2]);
				if((*noeud)->imageDivisee[i*2+1][j*2+1] < 0)
					(*noeud)->imageDivisee[i*2+1][j*2+1] = abs((*noeud)->imageDivisee[i*2+1][j*2+1]);
			}
		}
	}
}

// Libère proprement la totalité de l'arbre créé par decomposeNniveaux
void libereArbre(arbreImage **noeud) {
	if(*noeud != NULL) {
		// On descend jusqu'au feuille de l'arbre
		libereArbre(&((*noeud)->filsNO));
		libereArbre(&((*noeud)->filsNE));
		libereArbre(&((*noeud)->filsSO));
		libereArbre(&((*noeud)->filsSE));
		// Désallocation de la matrice imageDivisee
		if((*noeud)->imageDivisee != NULL)
			libereMatrice((*noeud)->ymax - (*noeud)->ymin, &((*noeud)->imageDivisee));
		// Désallocation du noeud avec affectation de la valeur NULL
		free(*noeud);
		*noeud = NULL;
	}
}

// Libère proprement le filsNO de l'arbre
void libereFilsNO(arbreImage **noeud) {
	if((*noeud)->imageDivisee != NULL && (*noeud)->filsNO->filsNO != NULL) {
		// Descend jusqu'au dernier filsNO de l'arbre
		libereFilsNO(&((*noeud)->filsNO));
		// Désallocation de la matrice imageDivisee
		libereMatrice((*noeud)->ymax - (*noeud)->ymin, &((*noeud)->imageDivisee));
	}
}
